package org.thread;

public class A {
	protected  String lock = "";
	public void a(){
		synchronized (lock) {
			System.out.println(Thread.currentThread().getName()+":a方法");
		}
	}
	
	public void b(){
		synchronized (lock) {
			System.out.println(Thread.currentThread().getName()+":b方法");
		}
	}
}
