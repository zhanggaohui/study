package cn.webank.framework.retry.model;

import cn.webank.framework.retry.dto.RetryMessageDTO;

public class RetryMessage {
	private RetryMessageDTO<?> retryMessageDto;
	public static final long ONE_DAY = 24 * 3600 * 1000;
	public static final long INTERVAL_MILLIS = 10 * 60 * 1000;

	public RetryMessage(RetryMessageDTO<?> retryMessageDto) {
		this.retryMessageDto = retryMessageDto;
	}

	public boolean isExpired() {
		if (retryMessageDto.getExpiredTime() < 0) {
			return false;
		} else {
			return (System.currentTimeMillis() - retryMessageDto
					.getExpiredTime()) > ONE_DAY;
		}
	}

	public boolean isOverThreshHold() {
		if (retryMessageDto.getMaxRetryTimes() < 0) {
			return false;
		} else {
			return (retryMessageDto.getRetryTimes().get() > retryMessageDto
					.getMaxRetryTimes());
		}
	}

	public boolean isEnableExecute() {
		long currentTime = System.currentTimeMillis();
		long lastExecutedTime = retryMessageDto.getLastExecutedTime();
		if ((currentTime - lastExecutedTime) > retryMessageDto.getRetryTimes()
				.get() * INTERVAL_MILLIS) {
			return true;
		} else {
			return false;
		}

	}
}