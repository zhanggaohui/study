package cn.webank.framework.retry.dto;

import java.lang.reflect.ParameterizedType;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 重试对象，请不要直接使用此对象，应该使用继承此对象的子类
 * <code>
 * 		public class MyRetryMessageDTO extends RetryMessageDTO&lt;BaseDTO&gt; {
 * 			......
 * 		}
 * 
 * 		MyRetryMessageDTO message = new MyRetryMessageDTO();
 * 		
 * </code>
 * 
 * @author jonyang
 * @param <T> 保存业务对象
 */
public class RetryMessageDTO<T> {

	private long expiredTime = -1;
	private AtomicInteger retryTimes = new AtomicInteger(0);

	private int maxRetryTimes = -1;
	private String key;
	private long lastExecutedTime = System.currentTimeMillis();
	private String retryObjectJson;
	private Class<T> retryObjectClass;

	@SuppressWarnings("unchecked")
	public RetryMessageDTO() {
		this.retryObjectClass = (Class<T>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0];
	}

	/**
	 * @return the lastExecutedTime
	 */
	public long getLastExecutedTime() {
		return lastExecutedTime;
	}

	/**
	 * @param lastExecutedTime
	 *            the lastExecutedTime to set
	 */
	public void setLastExecutedTime(long lastExecutedTime) {
		this.lastExecutedTime = lastExecutedTime;
	}

	public void addRetryTimes() {
		retryTimes.addAndGet(1);
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the retryTimes
	 */
	public AtomicInteger getRetryTimes() {
		return retryTimes;
	}

	/**
	 * @param retryTimes
	 *            the retryTimes to set
	 */
	public void setRetryTimes(AtomicInteger retryTimes) {
		this.retryTimes = retryTimes;
	}

	/**
	 * @return the maxRetryTimes
	 */
	public int getMaxRetryTimes() {
		return maxRetryTimes;
	}

	/**
	 * @param maxRetryTimes
	 *            the maxRetryTimes to set
	 */
	public void setMaxRetryTimes(int maxRetryTimes) {
		this.maxRetryTimes = maxRetryTimes;
	}

	/**
	 * @return the expiredTime
	 */
	public long getExpiredTime() {
		return expiredTime;
	}

	/**
	 * @param expiredTime
	 *            the expiredTime to set
	 */
	public void setExpiredTime(long expiredTime) {
		this.expiredTime = expiredTime;
	}

	/**
	 * @return the retryObjectJson
	 */
	public String getRetryObjectJson() {
		return retryObjectJson;
	}

	/**
	 * @param retryObjectJson
	 *            the retryObjectJson to set
	 */
	public void setRetryObjectJson(String retryObjectJson) {
		this.retryObjectJson = retryObjectJson;
	}

	/**
	 * @return the retryObjectClass
	 */
	public Class<?> getRetryObjectClass() {
		return retryObjectClass;
	}

	/**
	 * @param retryObjectClass
	 *            the retryObjectClass to set
	 */
	public void setRetryObjectClass(Class<T> retryObjectClass) {
		this.retryObjectClass = retryObjectClass;
	}

}
