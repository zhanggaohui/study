package cn.webank.framework.retry.integration;

import java.util.List;

import redis.clients.jedis.ShardedJedis;

public interface RetryDAO {

	public void saveRetryMessage(String namespace, String queue, String key,
			String json);

	public String getRetryMessage(String namespace, String queue,
			ShardedJedis jedis);

	public long getRetryMessageSize(String namespace, String queue,
			ShardedJedis jedis);

	public List<ShardedJedis> getJedises();
}
