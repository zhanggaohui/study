/**
 * Copyright (C) @2014 Webank Group Holding Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.webank.framework.seq.biz.service;

import java.util.Date;

public interface SeqService {

	public final static String BIZ_SEQ = "0";

	public final static String CONSUMER_SEQ = "1";

	/**
	 * 获取当前序列值
	 * 
	 * @param moduleId
	 *            模块id
	 * @param currentDate
	 *            当前时间
	 * @return 序列值
	 */
	public String nextValue(String moduleId, Date currentDate);

	/**
	 * 获取业务流水号
	 * 
	 * @param moduleId
	 *            模块id
	 * @param dcnNo
	 *            dcn号
	 * @param currentDate
	 *            当前时间
	 * @return 业务流水号
	 */
	public String nextBizSeqNo(String moduleId, String dcnNo, Date currentDate);

	/**
	 * 获取系统流水号
	 * 
	 * @param moduleId
	 *            模块id
	 * @param dcnNo
	 *            dcn号
	 * @param currentDate
	 *            当前时间
	 * @return 系统流水号
	 */
	public String nextConsumerSeqNo(String moduleId, String dcnNo,
			Date currentDate);

	/**
	 * @param moduleId 模块id
	 * @param dcnNo dcn号
	 * @param type
	 *            流水号类型
	 * @param currentDate 当前时间
	 * @return 流水号
	 */
	public String nextSeqNo(String moduleId, String dcnNo, char type,
			Date currentDate);
}
