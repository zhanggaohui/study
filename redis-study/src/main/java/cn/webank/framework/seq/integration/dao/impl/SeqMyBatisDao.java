/**
 * Copyright (C) @2014 Webank Group Holding Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.webank.framework.seq.integration.dao.impl;

import java.sql.SQLException;
import java.util.Map;

import org.springframework.stereotype.Repository;

import cn.webank.framework.integration.dao.session.WeBankSqlSession;
import cn.webank.framework.integration.dao.support.AbstractSimpleDao;
import cn.webank.framework.seq.integration.dao.SeqDao;
import cn.webank.framework.seq.model.Sequence;

/**
 * @author jonyang
 *
 */
@Repository("cn.webank.framework.seq.integration.dao.SeqDao")
public class SeqMyBatisDao extends AbstractSimpleDao implements SeqDao {
	@Override
	public Sequence queryOldValueByModuleId(String moduleId)
			throws SQLException {
		try (WeBankSqlSession session = getSession()) {
			SeqDao mapper = (SeqDao) session.getMapper(SeqDao.class);
			return mapper.queryOldValueByModuleId(moduleId);
		}
	}

	@Override
	public Sequence lockOldValueByModuleId(String moduleId) throws SQLException {
		try (WeBankSqlSession session = getSession()) {
			SeqDao mapper = (SeqDao) session.getMapper(SeqDao.class);
			return mapper.lockOldValueByModuleId(moduleId);
		}
	}

	@Override
	public int updateNewValue(Map<String, ?> parameterMap) throws SQLException {
		try (WeBankSqlSession session = getSession()) {
			SeqDao mapper = (SeqDao) session.getMapper(SeqDao.class);
			return mapper.updateNewValue(parameterMap);
		}
	}

	@Override
	public void insertValue(Sequence seq) throws SQLException {
		try (WeBankSqlSession session = getSession()) {
			SeqDao mapper = (SeqDao) session.getMapper(SeqDao.class);
			mapper.insertValue(seq);
		}
	}

	@Override
	public void deleteValueByModuleId(String moduleId) throws SQLException {
		try (WeBankSqlSession session = getSession()) {
			SeqDao mapper = (SeqDao) session.getMapper(SeqDao.class);
			mapper.deleteValueByModuleId(moduleId);
		}
	}

}
