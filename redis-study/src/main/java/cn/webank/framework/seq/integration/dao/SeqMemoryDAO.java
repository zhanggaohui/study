package cn.webank.framework.seq.integration.dao;

import java.util.Date;

import cn.webank.framework.seq.model.SequenceRange;

/**
 * 
 * @author jonyang
 *
 */
public interface SeqMemoryDAO {

	public SequenceRange getSeq(String namespace, String seqName,
			Date currentTime, int step);

	public boolean isExistsSeq(String namespace, String seqName);

	public boolean initSeq(String namespace, String seqName,
			long retryTimeoutMills, int effectSeconds);

}
