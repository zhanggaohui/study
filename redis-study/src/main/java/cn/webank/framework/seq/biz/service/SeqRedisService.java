/**
 * Copyright (C) @2014 Webank Group Holding Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.webank.framework.seq.biz.service;

import java.util.Date;

public interface SeqRedisService {

	public final static String BIZ_SEQ = "0";

	public final static String CONSUMER_SEQ = "1";

	/**
	 * 获取当前序列值
	 * 
	 * @param currentDate
	 *            当前时间
	 * @param namespace
	 *            redis 命名空间
	 * @param moduleId
	 *            redis 模块名字
	 * @return 序列
	 */
	public String nextValue(String namespace, String moduleId, Date currentDate);

	/**
	 * 获取业务流水号
	 * @param namespace redis 命名空间
	 * @param moduleId  redis 模块名字
	 * @param dcnNo dcn号
	 * @param currentDate 当前时间
	 * @return 业务流水号
	 */
	public String nextBizSeqNo(String namespace, String moduleId, String dcnNo,
			Date currentDate);

	/**
	 * 获取系统流水号
	 * @param namespace redis 命名空间
	 * @param moduleId  redis 模块名字
	 * @param dcnNo dcn号
	 * @param currentDate 当前时间
	 * @return 系统流水号
	 */
	public String nextConsumerSeqNo(String namespace, String moduleId,
			String dcnNo, Date currentDate);
}
