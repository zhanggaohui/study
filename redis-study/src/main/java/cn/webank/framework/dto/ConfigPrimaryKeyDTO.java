package cn.webank.framework.dto;

/**
 * 配置信息对象
 * 
 * @author jonyang
 *
 */
public class ConfigPrimaryKeyDTO extends BaseDTO {
	/**
	 * 系统id
	 */
	private String systemId;

	/**
	 * 配置项id
	 */
	private String itemId;
	
	
	/**
	 * dcn号
	 */
	private String dcnNo;

	
	/**
	 * @return the dcnNo
	 */
	public String getDcnNo() {
		return dcnNo;
	}

	/**
	 * @param dcnNo the dcnNo to set
	 */
	public void setDcnNo(String dcnNo) {
		this.dcnNo = dcnNo;
	}

	/**
	 * @return the systemId
	 */
	public String getSystemId() {
		return systemId;
	}

	/**
	 * @param systemId the systemId to set
	 */
	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	/**
	 * @return the itemId
	 */
	public String getItemId() {
		return itemId;
	}

	/**
	 * @param itemId
	 *            the itemId to set
	 */
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

}
