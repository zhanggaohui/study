package cn.webank.framework.dto;

public enum ValidationTypeEnum {

	NOTNULL("0"), SPECIFIC("1");

	private String value;

	ValidationTypeEnum(String v) {
		value = v;
	}

	public String getValue() {
		return value;
	}

}
