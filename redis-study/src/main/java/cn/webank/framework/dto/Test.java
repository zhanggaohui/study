package cn.webank.framework.dto;

import cn.webank.framework.mapper.JsonMapper;

//@JsonInclude(content=Include.NON_EMPTY)
public class Test {
	private int i = 0;

	public int getI() {
		return i;
	}

	public void setI(int i) {
		this.i = i;
	}

	public static void main(String[] args) {
		JsonMapper m = JsonMapper.nonEmptyMapper();
		System.out.println(m.toJson(new Test()));

	}

}
