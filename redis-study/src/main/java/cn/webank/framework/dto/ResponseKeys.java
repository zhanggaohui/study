package cn.webank.framework.dto;

public class ResponseKeys {
	public static final String SERVICE_NO_DEFINE_CODE = "9003";
	public static final String SYSTEM_ERROR_CODE = "9002";
	public static final String SYSTEM_TIMEOUT_CODE = "9001";
	public static final String SUCCESS_CODE = "0000";
	public static final String VALIDATION_ERROR_CODE = "V001";

}
