/**
 * Copyright (C) @2014 Webank Group Holding Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.webank.framework.dto;

import org.springframework.util.Assert;

/**
 * @author jonyang
 *
 */
public class PageInfo {
	public static final int DEFAULT_PAGE_SIZE = 10;
	public static final String MAP_KEY = "page";

	protected int pageSize = DEFAULT_PAGE_SIZE;
	protected int currentPage = 0;
	// protected int prePage;
	// protected int nextPage;
	protected int totalPage = -1;
	protected int totalCount = -1;

	/**
	 * @return the pageSize
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize
	 *            the pageSize to set
	 */
	public void setPageSize(int pageSize) {
		Assert.isTrue(pageSize >= 1, "page size must be larger than 1");
		this.pageSize = pageSize;
	}

	/**
	 * @return the currentPage
	 */
	public int getCurrentPage() {
		return currentPage;
	}

	/**
	 * @param currentPage
	 *            the currentPage to set
	 */
	public void setCurrentPage(int currentPage) {
		Assert.isTrue(pageSize >= 1, "current page must be larger than 1");
		this.currentPage = currentPage;
	}

	/**
	 * @return the totalPage
	 */
	public int getTotalPage() {
		return totalPage;
	}

	/**
	 * @param totalPage
	 *            the totalPage to set
	 */
	public void setTotalPage(int totalPage) {
		Assert.isTrue(pageSize >= 0,
				"total page must be larger than or equals to 0");
		this.totalPage = totalPage;
	}

	/**
	 * @return the totalCount
	 */
	public int getTotalCount() {
		return totalCount;
	}

	/**
	 * @param totalCount
	 *            the totalCount to set
	 */
	public void setTotalCount(int totalCount) {
		Assert.isTrue(pageSize >= 0,
				"total count must be larger than or equals to 0");
		this.totalCount = totalCount;
	}

	public PageInfo() {
		this.currentPage = 1;
		this.pageSize = DEFAULT_PAGE_SIZE;
	}

	/**
	 * 构造函数
	 * 
	 * @param currentPage
	 *            当前页
	 * @param pageSize
	 *            页数
	 */
	public PageInfo(int currentPage, int pageSize) {
		this.currentPage = currentPage;
		Assert.isTrue(pageSize >= 1, "page size must be larger than 1");
		this.pageSize = pageSize;

	}

}
