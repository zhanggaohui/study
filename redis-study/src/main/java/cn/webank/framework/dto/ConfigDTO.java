package cn.webank.framework.dto;

/**
 * 配置信息对象
 * 
 * @author jonyang
 *
 */
public class ConfigDTO extends BaseDTO {

	private ConfigPrimaryKeyDTO pk = new ConfigPrimaryKeyDTO();

	/**
	 * 配置内容
	 */
	private String content;

	/**
	 * md5串
	 */
	private String md5;

	/**
	 * @return the md5
	 */
	public String getMd5() {
		return md5;
	}

	/**
	 * @param md5
	 *            the md5 to set
	 */
	public void setMd5(String md5) {
		this.md5 = md5;
	}

	/**
	 * @return the sysId
	 */
	public String getSystemId() {
		return pk.getSystemId();
	}

	/**
	 * @param sysId
	 *            the sysId to set
	 */
	public void setSystemId(String sysId) {
		this.pk.setSystemId(sysId);
	}

	/**
	 * @return the itemId
	 */
	public String getItemId() {
		return pk.getItemId();
	}

	/**
	 * @param itemId
	 *            the itemId to set
	 */
	public void setItemId(String itemId) {
		this.pk.setItemId(itemId);
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

}
