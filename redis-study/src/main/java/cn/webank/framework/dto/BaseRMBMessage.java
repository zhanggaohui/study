/**
 * Copyright (C) @2014 Webank Group Holding Limited
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.webank.framework.dto;

import java.util.Date;
import java.util.List;

import cn.webank.framework.mapper.JsonMapper;
import cn.webank.rmb.destination.Destination;
import cn.webank.rmb.destination.SimpleDestination;
import cn.webank.rmb.message.AppHeader;
import cn.webank.rmb.message.Message;
import cn.webank.rmb.message.Ret;
import cn.webank.rmb.message.SysHeader;

//import cn.webank.rmb.destination.SimpleDestination;

abstract public class BaseRMBMessage<E> {

	protected final static String WEBANK_CORPORATION_ID = "99996";
	protected static JsonMapper jsonMapper = JsonMapper.nonEmptyMapper();
	protected Message rmbMessage;

	/**
	 * 获取场景号
	 *
	 * @return 场景号
	 */
	public String getScenario() {
		if (this.rmbMessage.getDestination() instanceof SimpleDestination) {
			SimpleDestination s = (SimpleDestination) this.rmbMessage.getDestination();
			return s.getScenario();
		} else {
			return null;
		}
	}

	/**
	 * 设置场景号
	 *
	 * @param scenario
	 *            场景号
	 */
	public void setScenario(String scenario) {
		if (this.rmbMessage.getDestination() instanceof SimpleDestination) {
			SimpleDestination s = (SimpleDestination) this.rmbMessage.getDestination();
			s.setScenario(scenario);
		} else {
			// String type = this.rmbMessage.getDestination().getType();
			// if (type.equals(Destination.DES_TYPE_SERVICE)) {
			// ServiceDestination s = (ServiceDestination) this.rmbMessage
			// .getDestination();
			// s.setScenario(scenario);
			// } else if (type.equals(Destination.DES_TYPE_EDA)) {
			// EventDestination e = (EventDestination) this.rmbMessage
			// .getDestination();
			// e.setScenario(scenario);
			// }

		}

	}

	/**
	 * 获取服务id
	 *
	 * @return 服务id
	 */
	public String getServiceId() {
		if (this.rmbMessage.getDestination() instanceof SimpleDestination) {
			SimpleDestination s = (SimpleDestination) this.rmbMessage.getDestination();
			return s.getServiceOrEventId();
		} else {
			return null;
		}

	}

	/**
	 * 设置服务id
	 *
	 * @param serviceId
	 *            服务id
	 */
	public void setServiceId(String serviceId) {

		if (this.rmbMessage.getDestination() instanceof SimpleDestination) {
			SimpleDestination s = (SimpleDestination) this.rmbMessage.getDestination();
			s.setServiceOrEventId(serviceId);
		}

	}

	/**
	 * 获取dcn号
	 *
	 * @return dcn号
	 */
	public String getDcnNo() {
		if (this.rmbMessage.getDestination() instanceof SimpleDestination) {
			SimpleDestination s = (SimpleDestination) this.rmbMessage.getDestination();
			return s.getDcnNo();
		} else {
			return null;
		}

	}

	/**
	 * 设置dcn号
	 *
	 * @param dcnNo
	 *            dcn号
	 */
	public void setDcnNo(String dcnNo) {
		if (this.rmbMessage.getDestination() instanceof SimpleDestination) {
			SimpleDestination s = (SimpleDestination) this.rmbMessage.getDestination();
			s.setDcnNo(dcnNo);
		}

	}

	/**
	 * 获取法人id
	 *
	 * @return 法人id
	 */
	public String getOrganizationId() {
		if (this.rmbMessage.getDestination() instanceof SimpleDestination) {
			SimpleDestination s = (SimpleDestination) this.rmbMessage.getDestination();
			return s.getOrganizationId();
		} else {
			return null;
		}
	}

	/**
	 * 设置法人id
	 *
	 * @param organizationId
	 *            法人id
	 */
	public void setOrganizationId(String organizationId) {
		if (this.rmbMessage.getDestination() instanceof SimpleDestination) {
			SimpleDestination s = (SimpleDestination) this.rmbMessage.getDestination();
			s.setOrganizationId(organizationId);
		}
	}

	/**
	 * 设置业务请求对象
	 *
	 * @param requestObject
	 *            业务请求对象
	 */
	public void setRequestObject(E requestObject) {
		if (requestObject == null) {
			this.rmbMessage.setContent(null);
		} else {
			this.rmbMessage.setContent(jsonMapper.toJson(requestObject));

		}
	}

	/**
	 * 获取系统头信息
	 *
	 * @return 系统头信息
	 */
	public SysHeader getSysHeader() {
		return rmbMessage.getSysHeader();
	}

	/**
	 * 获取rmb 消息
	 *
	 * @return rmb消息
	 */
	public Message getRmbMessage() {
		return rmbMessage;
	}

	/**
	 * 设置rmb消息
	 *
	 * @param rmbMessage
	 *            rmb消息
	 */
	public void setRmbMessage(Message rmbMessage) {
		this.rmbMessage = rmbMessage;
	}

	/**
	 * 获取应用头信息
	 *
	 * @return 应用头信息
	 */
	public AppHeader getAppHeader() {
		return rmbMessage.getAppHeader();
	}

	/**
	 * 创建目的地
	 *
	 * @return 返回目的地信息
	 */
	abstract public Destination createDestination();

	// sys header

	/**
	 * 获取业务流水号
	 *
	 * @return 业务流水号
	 */
	public String getBizSeqNo() {
		return getSysHeader().getBizSeqNo();
	}

	/**
	 * 设置业务流水号
	 *
	 * @param bizSeqNo
	 *            业务流水号
	 */
	public void setBizSeqNo(String bizSeqNo) {
		this.getSysHeader().setBizSeqNo(bizSeqNo);
	}

	/**
	 * 获取源系统id
	 *
	 * @return 源系统id
	 */
	public String getOrgSysId() {
		return getSysHeader().getOrgSysId();
	}

	/**
	 * 设置源系统id
	 *
	 * @param orgSysId
	 *            源系统id
	 */
	public void setOrgSysId(String orgSysId) {
		this.getSysHeader().setOrgSysId(orgSysId);
	}

	/**
	 * 获取当前调用系统id
	 *
	 * @return 当前调用系统id
	 */
	public String getConsumerId() {
		return this.getSysHeader().getConsumerId();
	}

	// /**
	// * 设置当前调用系统id
	// *
	// * @param consumerId
	// * 当前系统id
	// */
	// public void setConsumerId(String consumerId) {
	// this.getSysHeader().setConsumerId(consumerId);
	// }

	/**
	 * 获取当前交易时间
	 *
	 * @return 当前交易时间
	 */
	public Date getTranTimestamp() {
		return getSysHeader().getTranTimestamp();
	}

	/**
	 * 设置当前交易时间
	 *
	 * @param tranTimestamp
	 *            交易时间
	 */
	public void setTranTimestamp(Date tranTimestamp) {
		this.getSysHeader().setTranTimestamp(tranTimestamp);
	}

	/**
	 * 获取系统流水号
	 *
	 * @return 系统流水号
	 */
	public String getConsumerSeqNo() {
		return this.getSysHeader().getConsumerSeqNo();
	}

	/**
	 * 设置系统流水号
	 *
	 * @param consumerSeqNo
	 *            系统流水号
	 */
	public void setConsumerSeqNo(String consumerSeqNo) {
		this.getSysHeader().setConsumerSeqNo(consumerSeqNo);
	}

	/**
	 * 获取版本信息
	 *
	 * @return 版本信息
	 */
	public String getVersion() {
		return getSysHeader().getVersion();
	}

	/**
	 * 设置版本信息
	 *
	 * @param version
	 *            版本信息
	 */
	public void setVersion(String version) {
		this.getSysHeader().setVersion(version);
	}

	// app header
	// public String getTransCode() {
	// return appHeader.getTransCode();
	// }
	//
	// public void setTransCode(String transCode) {
	// this.appHeader.setTransCode(transCode);
	// }

	// public String getDepartmentId() {
	// return appHeader.getDepartmentId();
	// }
	//
	// public void setDepartmentId(String departmentId) {
	// this.appHeader.setDepartmentId(departmentId);
	// }

	// public String getUserId() {
	// return userId;
	// }
	//
	// public void setUserId(String userId) {
	// this.userId = userId;
	// }

	// public String getUserPassword() {
	// return userPassword;
	// }
	//
	// public void setUserPassword(String userPassword) {
	// this.userPassword = userPassword;
	// }

	// public String getUserLevel() {
	// return userLevel;
	// }
	//
	// public void setUserLevel(String userLevel) {
	// this.userLevel = userLevel;
	// }

	// public String getUserType() {
	// return userType;
	// }
	//
	// public void setUserType(String userType) {
	// this.userType = userType;
	// }

	// public String getApprFlag() {
	// return apprFlag;
	// }
	//
	// public void setApprFlag(String apprFlag) {
	// this.apprFlag = apprFlag;
	// }
	//
	// public String[] getApprUserIdArray() {
	// return apprUserIdArray;
	// }
	//
	// public void setApprUserIdArray(String[] apprUserIdArray) {
	// this.apprUserIdArray = apprUserIdArray;
	// }
	//
	// public String getAuthFlag() {
	// return authFlag;
	// }
	//
	// public void setAuthFlag(String authFlag) {
	// this.authFlag = authFlag;
	// }
	//
	// public String[] getAuthUserIdArray() {
	// return authUserIdArray;
	// }
	//
	// public void setAuthUserIdArray(String[] authUserIdArray) {
	// this.authUserIdArray = authUserIdArray;
	// }

	/**
	 * 获取合作伙伴
	 * 
	 * @return 合作伙伴
	 */
	public String getPartner() {
		return this.getAppHeader().getPartner();
	}

	/**
	 * 设置合作伙伴
	 * 
	 * @param partner
	 *            合作伙伴
	 */
	public void setPartner(String partner) {
		this.getAppHeader().setPartner(partner);
	}

	/**
	 * 获取反冲流水号
	 *
	 * @return 反冲流水号
	 */
	public String getReversalSeqNo() {
		return this.getAppHeader().getReversalSeqNo();
	}

	/**
	 * 设置反冲流水号
	 *
	 * @param reversalSeqNo
	 *            反冲流水号
	 */
	public void setReversalSeqNo(String reversalSeqNo) {
		this.getAppHeader().setReversalSeqNo(reversalSeqNo);

	}

	/**
	 * 获取用户语言
	 *
	 * @return 用户语言
	 */
	public String getUserLang() {
		return getAppHeader().getUserLang();
	}

	/**
	 * 设置用户语言
	 *
	 * @param userLang
	 *            用户语言
	 */
	public void setUserLang(String userLang) {
		this.getAppHeader().setUserLang(userLang);
	}

	/**
	 * 获取交易状态
	 *
	 * @return 交易状态
	 */
	public String getRetStatus() {
		return getAppHeader().getRetStatus();
	}

	/**
	 * 设置交易状态
	 *
	 * @param retStatus
	 *            交易状态
	 */
	public void setRetStatus(String retStatus) {
		this.getAppHeader().setRetStatus(retStatus);
	}

	/**
	 * 获取结果信息列表
	 *
	 * @return 结果信息列表
	 */
	public List<Ret> getRetList() {
		return getAppHeader().getRetList();
	}

	/**
	 * 设置结果信息列表
	 *
	 * @param list
	 *            结果信息列表
	 */
	public void setRetList(List<Ret> list) {
		this.getAppHeader().setRetList(list);
	}

	/**
	 * 是否成功
	 *
	 * @return true成功, false失败
	 */
	public boolean isSuccess() {
		return (AppHeader.RET_STATUS_SUCCESS.equalsIgnoreCase(this.getRetStatus()));
	}

}
