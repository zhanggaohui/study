/**
 * Copyright (C) @2014 Webank Group Holding Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package cn.webank.framework.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * 用于spring前端与页面前端通讯的java对象
 * 
 * @author yangjun
 * 
 */
public class WebMessage<T> {
	private T result;
	// private List<String> messages;
	private ResponseStatusEnum status;

	private List<RetDTO> retList;

	/**
	 * 业务流水号
	 */
	private String bizSeqNo;

	/**
	 * 提交因子（用于防重复提交）
	 */
	private String submitKey;

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}

	// public List<String> getMessages() {
	// return messages;
	// }
	//
	// public void addMessage(String message) {
	// if (this.messages == null) {
	// this.messages = new ArrayList<String>();
	// }
	//
	// this.messages.add(message);
	// }

	public ResponseStatusEnum getStatus() {
		return status;
	}

	public void setStatus(ResponseStatusEnum status) {
		this.status = status;
	}

	/**
	 * @return the retList
	 */
	public List<RetDTO> getRetList() {
		return retList;
	}

	/**
	 * @param retList the retList to set
	 */
	public void setRetList(List<RetDTO> retList) {
		this.retList = retList;
	}

	/**
	 * 增加错误信息
	 * 
	 * @param ret 返回值对象
	 */
	public void addRet(RetDTO ret) {
		if (this.retList == null) {
			this.retList = new ArrayList<RetDTO>();
		}
		this.retList.add(ret);
	}

	public String getBizSeqNo() {
		return bizSeqNo;
	}

	public void setBizSeqNo(String bizSeqNo) {
		this.bizSeqNo = bizSeqNo;
	}

	public String getSubmitKey() {
		return submitKey;
	}

	public void setSubmitKey(String submitKey) {
		this.submitKey = submitKey;
	}

}
