/**
 * Copyright (C) 2014 Webank Group Holding Limited
 */
package cn.webank.framework.dto;

/**
 * description: 返回前面错误信息
 *
 * @author calmanpan
 * @version 1.0
 *
 */
public class RetDTO extends BaseDTO {

	/**
	 * @Fields retCode : 错误码
	 */
	private String retCode;

	/**
	 * @Fields retMessage : 错误信息
	 */
	private String retMessage;

	/**
	 * @return the retCode
	 */
	public String getRetCode() {
		return retCode;
	}

	/**
	 * @param retCode
	 *            the retCode to set
	 */
	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}

	/**
	 * @return the retMessage
	 */
	public String getRetMessage() {
		return retMessage;
	}

	/**
	 * @param retMessage
	 *            the retMessage to set
	 */
	public void setRetMessage(String retMessage) {
		this.retMessage = retMessage;
	}

}
