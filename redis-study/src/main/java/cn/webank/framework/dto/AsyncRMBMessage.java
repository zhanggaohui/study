/**
 * Copyright (C) @2014 Webank Group Holding Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.webank.framework.dto;

import cn.webank.rmb.api.Util;
import cn.webank.rmb.destination.Destination;
import cn.webank.rmb.destination.SimpleDestination;
import cn.webank.rmb.message.AppHeader;
import cn.webank.rmb.message.SysHeader;

/**
 * 异步消息累
 * 
 * @author jonyang
 *
 * @param <E>
 *            业务请求对象类型
 */
public class AsyncRMBMessage<E> extends BaseRMBMessage<E> {

	private MessageMode mode;

	public AsyncRMBMessage() {
		this(MessageMode.PUBSUB);
	}

	public AsyncRMBMessage(MessageMode mode) {
		super();
		this.mode = mode;

		switch (mode) {
		case P2P:
			this.rmbMessage = Util.createReplyMessage(new SysHeader(),
					new AppHeader(), null, null);
			break;
		case PUBSUB:
			this.rmbMessage = Util.createMessage(new SysHeader(),
					new AppHeader(), createDestination(), null);
			break;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cn.webank.framework.dto.BaseRMBMessage#SimpleDestination()
	 */
	@Override
	public Destination createDestination() {
		return new SimpleDestination(this.WEBANK_CORPORATION_ID,null, null, null);
	}

	/**
	 * @return the mode
	 */
	public MessageMode getMode() {
		return mode;
	}

	/**
	 * @param mode
	 *            the mode to set
	 */
	public void setMode(MessageMode mode) {
		this.mode = mode;
	}

	public enum MessageMode {
		P2P, PUBSUB;
	}

	/**
	 * 设置事件Id
	 * 
	 * @param eventId
	 *            事件id
	 */
	public void setEventId(String eventId) {
		// if (this.rmbMessage.getDestination() instanceof EventDestination) {
		// EventDestination e = (EventDestination) this.rmbMessage
		// .getDestination();
		// e.setEventId(eventId);
		//
		// } else {
		if (this.rmbMessage.getDestination() instanceof SimpleDestination) {
			SimpleDestination e = (SimpleDestination) this.rmbMessage
					.getDestination();
			e.setServiceOrEventId(eventId);
		}
	}

}
