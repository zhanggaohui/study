package cn.webank.framework.dto;

public class PreServiceResultDTO<T> extends BaseDTO {
	private ResponseStatusEnum status;
	private T result;

	public ResponseStatusEnum getStatus() {
		return status;
	}

	public T getResult() {
		return result;
	}

	public void setStatus(ResponseStatusEnum status) {
		this.status = status;
	}

	public void setResult(T result) {
		this.result = result;
	}

	public static <E> PreServiceResultDTO<E> createDto() {
		PreServiceResultDTO<E> t = new PreServiceResultDTO<E>();
		return t;
	}

	public static void main(String[] args) throws Exception {
		PreServiceResultDTO<String> s = createDto();
		System.out.println(s.getStatus());
		System.out.println(s.getResult());
	}

}
