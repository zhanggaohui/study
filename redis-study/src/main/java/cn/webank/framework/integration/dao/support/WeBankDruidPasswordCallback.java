package cn.webank.framework.integration.dao.support;

import java.io.IOException;

import javax.security.auth.callback.PasswordCallback;

import cn.webank.framework.exception.SysException;
import cn.webank.framework.utils.WeBankEncryptUtil;

public class WeBankDruidPasswordCallback extends PasswordCallback {

	private static final long serialVersionUID = -1045581461664476782L;

	/**
	 * 发布平台的公钥证书文件
	 */
	private String sysPublicKeyFilePath;

	/**
	 * 应用私钥证书文件
	 */
	private String appPrivateKeyFilePath;

	/**
	 * 解密后的密码
	 */
	private String decryptPassword = null;

	/**
	 * @return the decryptPassword
	 */
	public String getDecryptPassword() {
		return decryptPassword;
	}

	/**
	 * @param decryptPassword
	 *            the decryptPassword to set
	 */
	public void setDecryptPassword(String decryptPassword) {
		this.decryptPassword = decryptPassword;
	}

	/**
	 * @return the sysPublicKeyFilePath
	 */
	public String getSysPublicKeyFilePath() {
		return sysPublicKeyFilePath;
	}

	/**
	 * @param sysPublicKeyFilePath
	 *            the sysPublicKeyFilePath to set
	 */
	public void setSysPublicKeyFilePath(String sysPublicKeyFilePath) {
		this.sysPublicKeyFilePath = sysPublicKeyFilePath;
	}

	/**
	 * @return the appPrivateKeyFilePath
	 */
	public String getAppPrivateKeyFilePath() {
		return appPrivateKeyFilePath;
	}

	/**
	 * @param appPrivateKeyFilePath
	 *            the appPrivateKeyFilePath to set
	 */
	public void setAppPrivateKeyFilePath(String appPrivateKeyFilePath) {
		this.appPrivateKeyFilePath = appPrivateKeyFilePath;
	}

	public WeBankDruidPasswordCallback(String prompt, boolean echoOn) {
		super(prompt, echoOn);
	}

	public WeBankDruidPasswordCallback() {
		this("webankDruidDataSource password", false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.security.auth.callback.PasswordCallback#clearPassword()
	 */
	@Override
	public void clearPassword() {
		super.clearPassword();
		this.decryptPassword = null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.security.auth.callback.PasswordCallback#getPassword()
	 */
	@Override
	public char[] getPassword() {
		if (decryptPassword == null) {
			String encryptPassword = new String(super.getPassword());
			try {
				decryptPassword = WeBankEncryptUtil.rsaDecrypt(
						sysPublicKeyFilePath, appPrivateKeyFilePath,
						encryptPassword);
			} catch (IOException e) {
				throw new SysException("decrypt password error", e);
			}
		}

		return decryptPassword.toCharArray();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.security.auth.callback.PasswordCallback#getPrompt()
	 */
	@Override
	public String getPrompt() {
		return super.getPrompt();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.security.auth.callback.PasswordCallback#isEchoOn()
	 */
	@Override
	public boolean isEchoOn() {
		return super.isEchoOn();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.security.auth.callback.PasswordCallback#setPassword(char[])
	 */
	@Override
	public void setPassword(char[] passwordChars) {
		super.setPassword(passwordChars);
	}

}
