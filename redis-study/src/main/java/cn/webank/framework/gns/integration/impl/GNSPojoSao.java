package cn.webank.framework.gns.integration.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import cn.webank.framework.exception.SysException;
import cn.webank.framework.gns.integration.GNSSao;
import cn.webank.gns.DCNInfo;
import cn.webank.gns.EcifIdInfo;
import cn.webank.gns.GNS;
import cn.webank.gns.GNSException;
import cn.webank.gns.util.Constants;
import cn.webank.gns.write.EcifWriteInfo;
import cn.webank.gns.write.GNSWrite;
import cn.webank.gns.write.GNSWriteException;

/**
 * GNS实现类 建议使用
 * 
 * Repository("cn.webank.framework.GNS.integration.GNSSao") BeanID变化
 * 
 * @author jonyang
 * 
 */
@Repository("cn.webank.framework.gns.integration.GNSSao")
public class GNSPojoSao implements GNSSao {

	private final static String WEBANK_CORPORATION_ID = "99996";

	@Override
	public String getDcnNoByEcifNo(String ecifNo) {
		try {
			return GNS.queryByEcifNo(ecifNo);
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}
	}

	@Override
	public String getDcnNoByCardNo(String cardNo) {
		try {
			return GNS.queryByBankCardNo(cardNo);
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cn.webank.framework.GNS.biz.service.GNSService#getDcnNoListByEcifNoList
	 * (java.util.List)
	 */
	@Override
	public List<String> getDcnNoListByEcifNoList(List<String> ecifNoList) {
		List<String> dcnList = new ArrayList<String>();
		try {
			Map<String, String> map = GNS.queryByEcifNo(ecifNoList.toArray(new String[0]));
			if (map != null) {
				for (String k : ecifNoList) {
					dcnList.add(map.get(k));
				}
			}

			return dcnList;
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cn.webank.framework.GNS.biz.service.GNSService#getDcnNoListByCardNoList
	 * (java.util.List)
	 */
	@Override
	public List<String> getDcnNoListByCardNoList(List<String> cardNoList) {
		List<String> dcnList = new ArrayList<String>();
		try {
			Map<String, String> map = GNS.queryByBankCardNo(cardNoList.toArray(new String[0]));

			if (map != null) {
				for (String k : cardNoList) {
					dcnList.add(map.get(k));
				}
			}

			return dcnList;
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}
	}

	@Override
	public List<String> getDcnNoListByWechatOpenIdList(List<String> wechatNoList, String organizationId) {
		List<String> dcnList = new ArrayList<String>();
		try {
			Map<String, String> map = GNS.queryByWechatOpenId(wechatNoList.toArray(new String[0]), organizationId);

			if (map != null) {
				for (String k : wechatNoList) {
					dcnList.add(map.get(k));
				}
			}

			return dcnList;
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cn.webank.framework.GNS.biz.service.GNSService#getDcnNoListByQQOpenIdList
	 * (java.util.List)
	 */
	@Override
	public List<String> getDcnNoListByQQNoList(List<String> qqNoList) {
		// List<String> dcnList = new ArrayList<String>();
		// try {
		// Map<String, String> map = GNS.queryByQQNo(qqNoList.toArray(new
		// String[0]));
		//
		// if (map != null) {
		// for (String k : qqNoList) {
		// dcnList.add(map.get(k));
		// }
		// }
		//
		// return dcnList;
		// } catch (GNSException e) {
		// throw new SysException("GNS error", e);
		// }
		return getDcnNoListByQQNoList(qqNoList, WEBANK_CORPORATION_ID);
	}

	@Override
	public List<String> getDcnNoListByQQNoList(List<String> qqNoList, String organizationId) {
		List<String> dcnList = new ArrayList<String>();
		try {
			Map<String, String> map = GNS.queryByQQNo(qqNoList.toArray(new String[0]), organizationId);

			if (map != null) {
				for (String k : qqNoList) {
					dcnList.add(map.get(k));
				}
			}

			return dcnList;
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cn.webank.framework.GNS.biz.service.GNSService#
	 * getDcnNoListByAccountNoList (java.lang.String)
	 */
	@Override
	public List<String> getDcnNoListByAccountNoList(List<String> accountNoList) {
		List<String> dcnList = new ArrayList<String>();
		try {
			Map<String, String> map = GNS.queryByBankAccount(accountNoList.toArray(new String[0]));

			if (map != null) {
				for (String k : accountNoList) {
					dcnList.add(map.get(k));
				}
			}

			return dcnList;
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cn.webank.framework.GNS.biz.service.GNSService#getAdminDcnNo()
	 */
	@Override
	public String getAdminDcnNo() {

		// try {
		// return GNS.getAdminDCN();
		// } catch (GNSException e) {
		// throw new SysException("GNS error", e);
		// }
		return getAdminDcnNo(WEBANK_CORPORATION_ID);
	}

	@SuppressWarnings("deprecation")
	@Override
	public String getAdminDcnNo(String organizationId) {
		try {
			return GNS.getAdminDCN(organizationId);
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public String getCommonAdminDcnNo() {
		try {
			return GNS.getCommonAdminDCN();
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cn.webank.framework.GNS.biz.service.GNSService#getAllDcnNos()
	 */
	@Override
	public List<String> getAllDcnNos() {
		List<String> dcnList = new ArrayList<String>();
		try {
			List<DCNInfo> allDCN = GNS.getAllDCN();
			if (allDCN != null) {
				for (DCNInfo d : allDCN) {
					dcnList.add(d.getDcn());
				}
			}
			return dcnList;
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}

	}

	// @Override
	// public String getDcnNoByWechatOpenId(String wechatNo) {
	// try {
	// return GNS.queryByWechatOpenId(wechatNo);
	// } catch (GNSException e) {
	// throw new SysException("GNS error", e);
	// }
	// }

	@Override
	public String getDcnNoByWechatOpenId(String wechatNo, String organizationId) {
		try {
			return GNS.queryByWechatOpenId(wechatNo, organizationId);
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}
	}

	@Override
	public String getDcnNoByQQNo(String qqNo) {
		try {
			return GNS.queryByQQNo(qqNo);

		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}
	}

	@Override
	public String getDcnNoByQQNo(String qqNo, String organizationId) {
		try {
			return GNS.queryByQQNo(qqNo, organizationId);

		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}
	}

	@Override
	public String getDcnNoByAccountNo(String accountNo) {
		try {
			return GNS.queryByBankAccount(accountNo);
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}
	}

	@Override
	public String registerEcifNo(String ecifNo) {
		try {
			return GNSWrite.registDCNForEcif(ecifNo);
		} catch (GNSWriteException e) {
			throw new SysException("GNS error", e);
		}
	}

	@Override
	public String getAvailableDCN() {
		try {
			return GNSWrite.getAvailableDCN();
		} catch (GNSWriteException e) {
			throw new SysException("GNS error", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cn.webank.framework.GNS.integration.GNSSao#getDcnNoByOpenId(java.lang
	 * .String)
	 */
	@Override
	public String getDcnNoByOpenId(String openId) {
		// try {
		// return GNS.queryByOpenId(openId);
		// } catch (GNSException e) {
		// throw new SysException("GNS error", e);
		// }
		return getDcnNoByOpenId(openId, WEBANK_CORPORATION_ID, Constants.APPIDQQ);
	}

	@Override
	public String getDcnNoByOpenId(String openId, String organizationId, int appId) {
		try {
			return GNS.queryByOpenId(openId, organizationId, appId);
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cn.webank.framework.GNS.integration.GNSSao#getDcnNoListByOpenIdList(java
	 * .util.List)
	 */
	@Override
	public List<String> getDcnNoListByOpenIdList(List<String> openIdList) {
		List<String> dcnList = new ArrayList<String>();
		try {
			Map<String, String> map = GNS.queryByOpenId(openIdList.toArray(new String[0]));
			if (map != null) {
				for (String k : openIdList) {
					dcnList.add(map.get(k));
				}
			}
			return dcnList;
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}
	}

	@Override
	public List<String> getDcnNoListByOpenIdList(List<String> openIdList, String organizationId, int appId) {
		List<String> dcnList = new ArrayList<String>();
		try {
			Map<String, String> map = GNS.queryByOpenId(openIdList.toArray(new String[0]), organizationId, appId);
			if (map != null) {
				for (String k : openIdList) {
					dcnList.add(map.get(k));
				}
			}
			return dcnList;
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public String getDMZ(String dcnNo) {
		try {
			return GNS.getDMZ(dcnNo);
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public String getECN(String dcnNo) {
		try {
			return GNS.getECN(dcnNo);
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}
	}

	@Override
	public String getByMoblie(String moblie) {
		// try {
		// return GNS.queryByMoblie(moblie);
		// } catch (GNSException e) {
		// throw new SysException("GNS error", e);
		// }
		return getByMoblie(moblie, WEBANK_CORPORATION_ID);
	}

	@Override
	public String getByMoblie(String moblie, String organizationId) {
		try {
			return GNS.queryByMoblie(moblie, organizationId);
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}
	}

	@Override
	public List<String> getByMoblie(List<String> mobiles, String organizationId) {
		List<String> dcnList = new ArrayList<String>();
		try {
			Map<String, String> map = GNS.queryByMoblie(mobiles.toArray(new String[0]), organizationId);
			if (map != null) {
				for (String k : mobiles) {
					dcnList.add(map.get(k));
				}
			}
			return dcnList;
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}
	}

	@Override
	public List<String> getCDCNByCcifNo(List<String> ccifNoes) {
		List<String> dcnList = new ArrayList<String>();
		Map<String, String> map;
		try {
			map = GNS.queryCDCNByCcifNo(ccifNoes.toArray(new String[0]));
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}

		if (map != null) {
			for (String k : ccifNoes) {
				dcnList.add(map.get(k));
			}
		}
		return dcnList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cn.webank.framework.gns.integration.GNSSao#registerEcifNo(java.lang.
	 * String , java.lang.String)
	 */
	@Override
	public String registerEcifNo(String ecifNo, String corporationId) {
		try {
			return GNSWrite.registDCNForEcif(ecifNo, corporationId);
		} catch (GNSWriteException e) {
			throw new SysException("GNS error", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cn.webank.framework.gns.integration.GNSSao#getAvailableDCN(java.lang.
	 * String)
	 */
	@Override
	public String getAvailableDCN(String corporationId) {
		try {
			return GNSWrite.getAvailableDCN(corporationId);
		} catch (GNSWriteException e) {
			throw new SysException("GNS error", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cn.webank.framework.gns.integration.GNSSao#addEcifInfo(cn.webank.gns.
	 * write.EcifWriteInfo)
	 */
	@Override
	public boolean addEcifInfo(EcifWriteInfo ecifInfo) {
		try {
			return GNSWrite.addEcifInfo(ecifInfo);
		} catch (GNSWriteException e) {
			throw new SysException("GNS error", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cn.webank.framework.gns.integration.GNSSao#changeDCN(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public String changeDCN(String ecifNo, String dcnNo) {
		try {
			return GNSWrite.changeDCN(ecifNo, dcnNo);
		} catch (GNSWriteException e) {
			throw new SysException("GNS error", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cn.webank.framework.gns.integration.GNSSao#changeDCN(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public String changeDCN(String ecifNo, String dcnNo, String corporationId) {
		try {
			return GNSWrite.changeDCN(ecifNo, dcnNo, corporationId);
		} catch (GNSWriteException e) {
			throw new SysException("GNS error", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cn.webank.framework.gns.integration.GNSSao#addEcifInfo(cn.webank.gns.
	 * write.EcifWriteInfo, java.lang.String)
	 */
	@Override
	public boolean addEcifInfo(EcifWriteInfo ecifInfo, String corporationId) {
		try {
			return GNSWrite.addEcifInfo(ecifInfo, corporationId);
		} catch (GNSWriteException e) {
			throw new SysException("GNS error", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cn.webank.framework.gns.integration.GNSSao#getDcnNoListByUnionId(java
	 * .lang.String)
	 */
	@Override
	public String getDcnNoByUnionId(String unionId) {
		// try {
		// return GNS.queryByUnionId(unionId);
		// } catch (GNSException e) {
		// throw new SysException("GNS error", e);
		// }
		return getDcnNoByUnionId(unionId, WEBANK_CORPORATION_ID);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cn.webank.framework.gns.integration.GNSSao#getDcnNoListByUnionId(java
	 * .lang.String, java.lang.String)
	 */
	@Override
	public String getDcnNoByUnionId(String unionId, String corporationIdId) {
		try {
			return GNS.queryByUnionId(unionId, corporationIdId);
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cn.webank.framework.gns.integration.GNSSao#getDcnNoListByUnionIdList(
	 * java.util.List)
	 */
	@Override
	public List<String> getDcnNoListByUnionIdList(List<String> unionIds) {
		// List<String> dcnList = new ArrayList<String>();
		// Map<String, String> map;
		// try {
		// map = GNS.queryByUnionId(unionIds.toArray(new String[0]));
		// } catch (GNSException e) {
		// throw new SysException("GNS error", e);
		// }
		//
		// if (map != null) {
		// for (String k : unionIds) {
		// dcnList.add(map.get(k));
		// }
		// }
		// return dcnList;
		return getDcnNoListByUnionIdList(unionIds, WEBANK_CORPORATION_ID);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cn.webank.framework.gns.integration.GNSSao#getDcnNoListByUnionIdList(
	 * java.util.List, java.lang.String)
	 */
	@Override
	public List<String> getDcnNoListByUnionIdList(List<String> unionIds, String corporationIdId) {
		List<String> dcnList = new ArrayList<String>();
		Map<String, String> map;
		try {
			map = GNS.queryByUnionId(unionIds.toArray(new String[0]), corporationIdId);
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}

		if (map != null) {
			for (String k : unionIds) {
				dcnList.add(map.get(k));
			}
		}
		return dcnList;
	}

	@Override
	public String getDcnNoByCoopAccount(String coopType, String coopAccount, String org) {
		try {
			return GNS.queryByCoopAccount(coopType, coopAccount, org);
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}
	}

	@Override
	public String getDcnNoByCoopAccount(String coopType, String coopAccount) {
		try {
			return GNS.queryByCoopAccount(coopType, coopAccount);
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}
	}

	@Override
	public String getDcnNoByQQOpenId(String qqOpenId, String corporationId) {
		try {
			return GNS.queryByQQOpenId(qqOpenId, corporationId);
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}
	}

	@Override
	public String getDcnNoByQQOpenId(String qqOpenId) {
		// TODO Auto-generated method stub
		try {
			return GNS.queryByQQOpenId(qqOpenId);
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}
	}

	@Override
	public String getDcnNoByEcifId(String idType, String idValue, String corporationId) {
		try {
			return GNS.queryByEcifId(idType, idValue, corporationId);
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}
	}

	@Override
	public List<String> getDcnNoByEcifIdList(List<EcifIdInfo> idList, String corporationId) {
		List<String> dcnList = new ArrayList<String>();
		Map<String, String> map;
		try {
			map = GNS.queryByEcifId(idList, corporationId);
		} catch (GNSException e) {
			throw new SysException("GNS error", e);
		}

		if (map != null) {
			for (EcifIdInfo k : idList) {
				dcnList.add(map.get(k.getValue()));// key是证件号
			}
		}
		return dcnList;
	}

}
