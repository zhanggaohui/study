package cn.webank.framework.gns.integration;

import java.util.List;

import cn.webank.gns.EcifIdInfo;
import cn.webank.gns.write.EcifWriteInfo;

public interface GNSSao {
	/**
	 * 根据客户号获取dcn号
	 * 
	 * @param ecifNo
	 *            客户号
	 * @return dcn号
	 */
	public String getDcnNoByEcifNo(String ecifNo);

	/**
	 * 批量根据客户号获取dcn号
	 * 
	 * @param ecifNoList
	 *            客户号列表
	 * @return dcn号列表
	 */
	public List<String> getDcnNoListByEcifNoList(List<String> ecifNoList);

	/**
	 * 根据卡号获取dcn号
	 * 
	 * @param cardNo
	 *            卡号
	 * @return dcn号
	 */
	public String getDcnNoByCardNo(String cardNo);

	/**
	 * 批量根据卡号获取dcn号
	 * 
	 * @param cardNoList
	 *            卡号列表
	 * @return dcn号列表
	 */
	public List<String> getDcnNoListByCardNoList(List<String> cardNoList);

	/**
	 * 根据openid获取dcnf号
	 * 
	 * @param openId
	 *            qqopenid
	 * @return dcn号
	 */
	public String getDcnNoByOpenId(String openId);

	/**
	 * 根据openid获取dcn号
	 * 
	 * @param openId
	 *            openid 不区分渠道qq或微信
	 * @param corporationId
	 *            法人号
	 * @param appId
	 *            公众号appid Constants.APPIDQQ/Constants.APPIDWECHAT
	 * @return dcn号
	 */
	public String getDcnNoByOpenId(String openId, String corporationId, int appId);

	/**
	 * 批量 根openid获取dcn号
	 * 
	 * @param openIdList
	 *            openid列表
	 * @return dcn号列表
	 */
	public List<String> getDcnNoListByOpenIdList(List<String> openIdList);

	/**
	 * 根据openid获取dcn号
	 * 
	 * @param wechatOpenId
	 *            wechatOPenid
	 * @return dcn号
	 */
	// public String getDcnNoByWechatOpenId(String wechatOpenId);

	/**
	 * 根据openid获取dcn号
	 * 
	 * @param wechatOpenId
	 *            wechatOPenid
	 * @param corporationId
	 *            法人id
	 * @return dcn号
	 */
	public String getDcnNoByWechatOpenId(String wechatOpenId, String corporationId);

	/**
	 * 批量 微信号列表获取dcn号列表
	 * 
	 * @param wechatOpenIdList
	 *            微信openid列表
	 * @return dcn号列表
	 */
	// public List<String> getDcnNoListByWechatOpenIdList(List<String>
	// wechatOpenIdList);

	/**
	 * 批量 微信号列表获取dcn号列表
	 * 
	 * @param wechatOpenIdList
	 *            微信openid列表
	 * @param corporationId
	 *            法人号
	 * @return dcn号列表
	 */
	public List<String> getDcnNoListByWechatOpenIdList(List<String> wechatOpenIdList, String corporationId);

	/**
	 * 根据QQ号获取dcn号
	 * 
	 * @param qqNo
	 *            qq号
	 * @return dcn号
	 */
	public String getDcnNoByQQNo(String qqNo);

	/**
	 * 根据QQ号获取dcn号
	 * 
	 * @param qqNo
	 *            qq号
	 * @param corporationId
	 *            法人号
	 * @return dcn号
	 */
	public String getDcnNoByQQNo(String qqNo, String corporationId);

	/**
	 * 批量根据QQ号获取dcn号
	 * 
	 * @param qqNoList
	 *            qq号列表
	 * @return dcn号列表
	 */
	public List<String> getDcnNoListByQQNoList(List<String> qqNoList);

	/**
	 * 批量根据QQ号获取dcn号
	 * 
	 * @param qqNoList
	 *            qq号列表
	 * @param corporationId
	 *            法人号
	 * @return dcn号列表
	 */
	public List<String> getDcnNoListByQQNoList(List<String> qqNoList, String corporationId);

	/**
	 * 根据账户获取dcn号
	 * 
	 * @param accountNo
	 *            账户
	 * @return dcn号
	 */
	public String getDcnNoByAccountNo(String accountNo);

	/**
	 * 批量根据账户获取dcn号
	 * 
	 * @param accountNoList
	 *            账户列表
	 * @return dcn号列表
	 */
	public List<String> getDcnNoListByAccountNoList(List<String> accountNoList);

	/**
	 * 往GNS注册新客户号
	 * 
	 * @param ecifNo
	 *            客户号
	 * @return dcn号
	 * @see getAvailableDCN
	 */
	public String registerEcifNo(String ecifNo);

	/**
	 * 往GNS注册新客户号
	 * 
	 * @param ecifNo
	 *            客户号
	 * @param corporationId
	 *            法人号
	 * @return dcn号
	 */
	public String registerEcifNo(String ecifNo, String corporationId);

	/**
	 * 获取可用的dcn号
	 * 
	 * @return dcn号
	 */
	public String getAvailableDCN();

	/**
	 * 获取可用的dcn号
	 * 
	 * @param corporationId
	 *            法人号
	 * @return dcn号
	 */
	public String getAvailableDCN(String corporationId);

	/**
	 * 添加GNS中银行卡号、银行账号、qq号、微信账号、用户ID信息(只要不为空的都会添加)
	 * 
	 * @param ecifInfo
	 *            包含银行卡号、银行账号、qq号、微信账号、用户ID信息的EcifInfo
	 * @return true or false
	 */
	public boolean addEcifInfo(EcifWriteInfo ecifInfo);

	/**
	 * 把EcifNo用户迁移到DCNNo,数据迁移时使用
	 * 
	 * @param ecifNo
	 *            Ecif号
	 * @param dcnNo
	 *            DCN号
	 * @return 新的DCN号
	 */

	public String changeDCN(String ecifNo, String dcnNo);

	/**
	 * 把EcifNo用户迁移到DCNNo,数据迁移时使用
	 * 
	 * @param ecifNo
	 *            Ecif号
	 * @param dcnNo
	 *            DCN号
	 * @param corporationId
	 *            法人号
	 * @return 新的DCN号
	 */
	public String changeDCN(String ecifNo, String dcnNo, String corporationId);

	/**
	 * 
	 * @param ecifInfo
	 *            包含银行卡号、银行账号、qq号、微信账号、用户ID信息的EcifInfo
	 * @param corporationId
	 *            法人
	 * @return true or false
	 */
	public boolean addEcifInfo(EcifWriteInfo ecifInfo, String corporationId);

	/**
	 * 获取管理DCN号
	 * 
	 * @return dcn号
	 */
	public String getAdminDcnNo();

	/**
	 * 获取指定法人的admin区dcn号
	 * 
	 * @param corporationId
	 *            法人号
	 * @return dcn号
	 */
	public String getAdminDcnNo(String corporationId);

	/**
	 * 获取跨法人的admin区dcn号
	 * 
	 * @return dcn号
	 */
	public String getCommonAdminDcnNo();

	/**
	 * 获取所有DCN号
	 * 
	 * @return dcn号列表
	 */
	public List<String> getAllDcnNos();

	/**
	 * 根据服务当前所在dcn号获取DMZ对应的dcn号
	 * 
	 * @param dcnNo
	 *            dcn号
	 * @return DMZ的dcn号
	 */
	public String getDMZ(String dcnNo);

	/**
	 * 获取ECN的dcn号
	 * 
	 * @param dcnNo
	 *            dcn号
	 * @return ECN的dcn号
	 */
	public String getECN(String dcnNo);

	// /**
	// * 根据用户ID获取dcn号
	// *
	// * @param userID
	// * 用户id
	// * @param corporationId
	// * 法人号
	// * @return dcn号
	// */
	// public String getByUserID(String userID, String corporationId);
	//
	// /**
	// * 根据用户ID获取dcn号
	// *
	// * @param userID
	// * 用户id
	// * @return dcn号
	// */
	// public String getByUserID(String userID);

	/**
	 * 根据电话号码查询DCN
	 * 
	 * @param moblie
	 *            手机号
	 * @return dcn号
	 */
	public String getByMoblie(String moblie);

	/**
	 * 根据电话号码查询DCN
	 * 
	 * @param moblie
	 *            手机号
	 * @param corporationId
	 *            法人号
	 * @return dcn号
	 */
	public String getByMoblie(String moblie, String corporationId);

	/**
	 * 批量根据电话号码查询DCN号
	 * 
	 * @param mobiles
	 *            手机号
	 * @param corporationId
	 *            法人号
	 * @return dcn号列表
	 */
	public List<String> getByMoblie(List<String> mobiles, String corporationId);

	/**
	 * 批量根据OpenID查询DCN号
	 * 
	 * @param openIdList
	 *            openid列表
	 * @param corporationId
	 *            法人号
	 * @param appId
	 *            公众号appId appId（Constants.APPIDQQ/Constants.APPIDWECHAT）
	 * @return dcn号列表
	 */
	public List<String> getDcnNoListByOpenIdList(List<String> openIdList, String corporationId, int appId);

	// /**
	// * 获取Ccif CDCN
	// *
	// * @param orgNo
	// * 组织机构号
	// * @return dcn号
	// */
	// public String getCDCNByOrgNo(String orgNo);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param orgNo
	// * 组织机构号
	// * @param corporationId
	// * 法人号
	// * @return dcn号
	// */
	// public String getCDCNByOrgNo(String orgNo, String corporationId);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param bizPermNo
	// * 营业执照号
	// * @return CDCN
	// */
	// public String getCDCNByBizPermNo(String bizPermNo);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param bizPermNo
	// * 营业执照号
	// * @param corporationId
	// * 法人号
	// * @return CDCN
	// */
	// public String getCDCNByBizPermNo(String bizPermNo, String corporationId);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param finLicNo
	// * 金融许可证号
	// * @return CDCN
	// */
	// public String getCDCNByFinLicNo(String finLicNo);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param finLicNo
	// * 金融许可证号
	// * @param corporationId
	// * 法人号
	// * @return CDCN
	// */
	// public String getCDCNByFinLicNo(String finLicNo, String corporationId);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param entCode
	// * 企业代码证号
	// * @return CDCN
	// */
	// public String getCDCNByEntCode(String entCode);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param entCode
	// * 企业代码证号
	// * @param corporationId
	// * 法人号
	// * @return CDCN 号
	// */
	// public String getCDCNByEntCode(String entCode, String corporationId);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param swiftCode SWIFT代码
	// * @return CDCN
	// */
	// public String getCDCNBySwiftCode(String swiftCode);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param swiftCode
	// * swift代码
	// * @param corporationId
	// * 法人号
	// * @return CDCN
	// */
	// public String getCDCNBySwiftCode(String swiftCode, String corporationId);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param qqNo qq号
	// * @return CDCN
	// */
	// public String getCDCNByQQNo(String qqNo);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param qqNo qq号
	// * @param corporationId
	// * 法人号
	// * @return CDCN
	// */
	// public String getCDCNByQQNo(String qqNo, String corporationId);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param wechatAccount 微信号
	// * @return CDCN
	// */
	// public String getCDCNByWechatAccount(String wechatAccount);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param wechatAccount 微信号
	// * @param corporationId
	// * 法人号
	// * @return CDCN
	// */
	// public String getCDCNByWechatAccount(String wechatAccount,
	// String corporationId);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param ccifNo
	// * ccif号
	// * @return CDCN 号
	// */
	// public String getCDCNByCcifNo(String ccifNo);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param orgNoes
	// * 组织机构号
	// * @return CDCN
	// */
	// public List<String> getCDCNByOrgNo(List<String> orgNoes);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param orgNoes
	// * 组织机构号
	// * @param corporationId
	// * 法人号
	// * @return CDCN
	// */
	// public List<String> getCDCNByOrgNo(List<String> orgNoes,
	// String corporationId);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param bizPermNos
	// * 营业执照号
	// * @return CDCN
	// */
	// public List<String> getCDCNByBizPermNo(List<String> bizPermNos);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param bizPermNoes
	// * 营业执照号
	// * @param corporationId
	// * 法人号
	// * @return CDCN
	// */
	// public List<String> getCDCNByBizPermNo(List<String> bizPermNoes,
	// String corporationId);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param finLicNoes
	// * 金融许可证号
	// * @return CDCN
	// */
	// public List<String> getCDCNByFinLicNo(List<String> finLicNoes);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param finLicNoes
	// * 金融许可证号
	// * @param corporationId
	// * 法人号
	// * @return CDCN
	// */
	// public List<String> getCDCNByFinLicNo(List<String> finLicNoes,
	// String corporationId);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param entCodes
	// * 企业代码证号列表
	// * @return CDCN
	// */
	// public List<String> getCDCNByEntCode(List<String> entCodes);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param entCodes
	// * 企业代码证号列表
	// * @param corporationId
	// * 法人号
	// * @return CDCN
	// */
	// public List<String> getCDCNByEntCode(List<String> entCodes,
	// String corporationId);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param swiftCodes
	// * SWIFTCODE列表
	// * @return CDCN
	// */
	// public List<String> getCDCNBySwiftCode(List<String> swiftCodes);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param swiftCodes
	// * SWIFTCODE列表
	// * @param corporationId
	// * 法人号
	// * @return CDCN
	// */
	// public List<String> getCDCNBySwiftCode(List<String> swiftCodes,
	// String corporationId);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param qqNoes
	// * qq号列表
	// * @return CDCN
	// */
	// public List<String> getCDCNByQQNo(List<String> qqNoes);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param qqNoes
	// * qq号列表
	// * @param corporationId
	// * 法人号
	// * @return CDCN
	// */
	// public List<String> getCDCNByQQNo(List<String> qqNoes,
	// String corporationId);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param wechatAccounts
	// * 微信号列表
	// * @return CDCN
	// */
	// public List<String> getCDCNByWechatAccount(List<String> wechatAccounts);
	//
	// /**
	// * 获取Ccif CDCN
	// *
	// * @param wechatAccounts
	// * 微信号列表
	// * @param corporationId
	// * 法人号
	// * @return CDCN
	// */
	// public List<String> getCDCNByWechatAccount(List<String> wechatAccounts,
	// String corporationId);

	/**
	 * 获取Ccif CDCN
	 * 
	 * @param ccifNos
	 *            ccif号列表
	 * @return CDCN
	 */
	public List<String> getCDCNByCcifNo(List<String> ccifNos);

	/**
	 * 根据微信unionid查询dcn号
	 * 
	 * @param unionId
	 *            微信unionid
	 * @return dcn号
	 */
	public String getDcnNoByUnionId(String unionId);

	/**
	 * 根据微信unionid查询dcn号
	 * 
	 * @param unionId
	 *            微信unionid
	 * @param corporationIdId
	 *            法人号
	 * @return dcn号
	 */
	public String getDcnNoByUnionId(String unionId, String corporationIdId);

	/**
	 * 根据微信unionid查询dcn号列表
	 * 
	 * @param unionIds
	 *            微信unionid 列表
	 * @return dcn号列表
	 */
	public List<String> getDcnNoListByUnionIdList(List<String> unionIds);

	/**
	 * 根据微信unionid查询dcn号列表
	 * 
	 * @param unionIds
	 *            微信unionid 列表
	 * @param corporationIdId
	 *            法人号
	 * @return dcn号列表
	 */
	public List<String> getDcnNoListByUnionIdList(List<String> unionIds, String corporationIdId);

	/**
	 * 根据合作方账号查询dcn号
	 * 
	 * @param coopType
	 *            合作方账户类型
	 * @param coopAccount
	 *            合作方账号
	 * @param org
	 *            法人号
	 * @return dcn号
	 */
	public String getDcnNoByCoopAccount(String coopType, String coopAccount, String org);

	/**
	 * 根据合作方账号查询dcn号
	 * 
	 * @param coopType
	 *            合作方账户类型
	 * @param coopAccount
	 *            合作方账号
	 * @return dcn号
	 */
	public String getDcnNoByCoopAccount(String coopType, String coopAccount);

	/**
	 * 根据qqopenid和法人号查询dcn
	 * @param qqOpenId qqOpenId
	 * @param corporationId 法人号
	 * @return dcn号
	 */
	public String getDcnNoByQQOpenId(String qqOpenId, String corporationId);

	/**
	 * 根据qq openid 查询dcn，默认法人为webank
	 * 
	 * @param qqOpenId qqOpenId
	 * @return dcn号
	 */
	public String getDcnNoByQQOpenId(String qqOpenId);

	/**
	 * 根据证件查询dcn
	 * 
	 * @param idType
	 *            证件类型
	 * @param idValue
	 *            证件号
	 * @param corporationId
	 *            法人号
	 * @return dcn号
	 */
	public String getDcnNoByEcifId(String idType, String idValue, String corporationId);

	/**
	 * 根据证件信息列表，查询dnc号列表
	 * @param idList 证件
	 * @param corporationId 法人号
	 * @return dcn号列表
	 */
	public List<String> getDcnNoByEcifIdList(List<EcifIdInfo> idList, String corporationId);

}
