package cn.webank.framework.gns.integration.support;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.webank.gns.util.GNSUtil;
import cn.webank.rmb.api.RMB;
import cn.webank.rmb.api.Request;
import cn.webank.rmb.api.Response;
import cn.webank.rmb.api.Util;
import cn.webank.rmb.destination.Destination;
import cn.webank.rmb.message.AppHeader;
import cn.webank.rmb.message.Message;
import cn.webank.rmb.message.SysHeader;

public class GNSRMBRequestorWrapper {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(GNSRMBRequestorWrapper.class);
	// 交易发起渠道，如网银、手机银行等，长度为4位的字符串(必填)
	private final static String sourceSysId = "GNS1";

	// 目标dcn(必填)
	// private final static String targetDCN = DCN.getCSDcn();

	// service id
	private final static String serviceId = "12300002";

	// 服务场景
	private final static String scenario = "01";

	// app header
	private final static AppHeader appHeader = new AppHeader();

	public static Response send(byte[] bytes, String csDcnNo) {
		try {
			// 1. 创建消息头
			String bizSeqNo = UUID.randomUUID().toString().replace("-", "");
			LOGGER.debug("Send request to gns server,bizSeqNo:" + bizSeqNo);
			SysHeader msgHeader = Util.createSysHeader(bizSeqNo, bizSeqNo,
					sourceSysId);
			// 2. 根据serviceId、targetDCN、scenario创建destination
			Destination destination = Util.createSimpleDestination(serviceId,
					scenario, csDcnNo);
			Message textMessage = Util.createMessage(msgHeader, appHeader,
					destination, new String(bytes));
			// 4. 根据超时时间与消息实例，创建发送请求对象
			long timeout = GNSUtil.getRMBInvokeTimeout();
			// System.out.println("@@@@@@@@@@@@@@@@@@@@@timeout:" + timeout);
			Request simpleRequest = Util.createRequest(textMessage, timeout);
			// 5. 发送消息并等待返回结果
			Response response = RMB.sendRequest(simpleRequest);
			return response;
		} catch (Exception e) {
			LOGGER.error("error when send", e);
			return null;
		}
	}
}
