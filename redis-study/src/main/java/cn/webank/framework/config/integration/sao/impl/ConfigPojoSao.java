/**
 * Copyright (C) @2014 Webank Group Holding Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.webank.framework.config.integration.sao.impl;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import cn.webank.framework.config.integration.sao.ConfigSao;
import cn.webank.framework.dto.ConfigDTO;
import cn.webank.framework.exception.SysException;
import cn.webank.framework.mapper.JsonMapper;

@Repository("cn.webank.framework.config.integration.sao.ConfigSao")
public class ConfigPojoSao implements ConfigSao {
	private final static Logger LOG = LoggerFactory
			.getLogger(ConfigPojoSao.class);
	private String host;
	private String port;
	private String adminDcnNo;

	private static JsonMapper jsonMapper = JsonMapper.nonEmptyMapper();

	public ConfigDTO getConfig(String sysId, String dcnNo, String itemId) {
		Assert.notNull(host, "host is null");
		Assert.notNull(port, "port is null");
		Assert.notNull(adminDcnNo, "adminDcnNo is null");

		URI uri = null;
		try {
			uri = new URIBuilder()
					.setScheme("http")
					.setHost(host + ":" + port)
					.setPath(
							"/cmc-cc/rest/content/" + sysId + "/" + itemId
									+ "/" + dcnNo).build();
		} catch (URISyntaxException e1) {
			throw new SysException("uri exception", e1);
		}

		CloseableHttpClient httpclient = HttpClients.createDefault();
		RequestConfig requestConfig = RequestConfig.custom()
				.setSocketTimeout(2000).setConnectTimeout(2000).build();// 设置请求和传输超时时间
		HttpGet httpget = new HttpGet(uri);
		httpget.setConfig(requestConfig);
		CloseableHttpResponse response;
		try {
			response = httpclient.execute(httpget);
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					String entityString = EntityUtils.toString(entity, "UTF-8");
					return jsonMapper.fromJson(entityString, ConfigDTO.class);
				}
			} else {
				LOG.error("getConfig http error code:"
						+ response.getStatusLine().getStatusCode());
				return null;
			}
		} catch (IOException e) {
			throw new SysException("io exception", e);
		} finally {

			try {
				if (httpclient != null) {
					httpclient.close();
				}
			} catch (IOException e) {
				throw new SysException("io exception", e);
			}

		}

		return null;

	}

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param host
	 *            the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return the port
	 */
	public String getPort() {
		return port;
	}

	/**
	 * @param port
	 *            the port to set
	 */
	public void setPort(String port) {
		this.port = port;
	}

	/**
	 * @return the adminDcnNo
	 */
	public String getAdminDcnNo() {
		return adminDcnNo;
	}

	/**
	 * @param adminDcnNo
	 *            the adminDcnNo to set
	 */
	public void setAdminDcnNo(String adminDcnNo) {
		this.adminDcnNo = adminDcnNo;
	}
}
