/**
 * Copyright (C) @2014 Webank Group Holding Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.webank.framework.config.integration.sao;

import cn.webank.framework.dto.ConfigDTO;

public interface ConfigSao {
	public ConfigDTO getConfig(String sysId, String dcnNo, String itemId);

	// public RMBSao getRmbSao();

	// public void setRmbSao(RMBSao rmbSao);

	// public GNSSao getGnsSao();

	// public void setGnsSao(GNSSao gnsSao);

	/**
	 * @return the host
	 */
	public String getHost();

	/**
	 * @param host
	 *            the host to set
	 */
	public void setHost(String host);

	/**
	 * @return the port
	 */
	public String getPort();

	/**
	 * @param port
	 *            the port to set
	 */
	public void setPort(String port);

	/**
	 * @return the adminDcnNo
	 */
	public String getAdminDcnNo();

	/**
	 * @param adminDcnNo
	 *            the adminDcnNo to set
	 */
	public void setAdminDcnNo(String adminDcnNo);
}
