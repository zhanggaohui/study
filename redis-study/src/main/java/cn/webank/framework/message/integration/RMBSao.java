package cn.webank.framework.message.integration;

import java.util.concurrent.TimeUnit;

import cn.webank.framework.dto.AsyncRMBMessage;
import cn.webank.framework.dto.SyncRMBMessage;
import cn.webank.rmb.core.intf.ISubscriber;

public interface RMBSao {

	/**
	 * 同步发送消息，等待返回结果
	 * 
	 * @param message
	 *            同步消息
	 * @param timeout
	 *            超时时间
	 * @param unit
	 *            超时时间单位
	 * @param <E>
	 *            请求业务对象
	 * @param <T>
	 *            响应业务对象
	 * @return 业务对象
	 */
	public <E, T> SyncRMBMessage<E, T> send(SyncRMBMessage<E, T> message,
			long timeout, TimeUnit unit);

	/**
	 * 异步发送消息，不等待结果
	 * 
	 * @param message
	 *            异步消息
	 * @param <E>
	 *            请求业务对象
	 */
	public <E> void publish(AsyncRMBMessage<E> message);

	/**
	 * 取消订阅者
	 * 
	 * @param subscriber
	 *            订阅者
	 */
	public void deregisterSubscriber(ISubscriber subscriber);

	/**
	 * 增加订阅者
	 * 
	 * @param subscriber
	 *            订阅者
	 */
	public void registerSubscriber(ISubscriber subscriber);
	
	
	/**
	 * 清理RMB资源
	 */
	public void destroy();

}
