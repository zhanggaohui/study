package cn.webank.framework.biz.server;

/**
 * Server命令类型
 * 
 * @author jonyang
 *
 */
public enum ServerCommandEnum {
	START("start"), STOP("stop"), HELP("help");

	private String value;

	ServerCommandEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	public static ServerCommandEnum getServerCommandEnum(String value) {
		for (ServerCommandEnum e : values()) {
			if (e.getValue().equals(value)) {
				return e;
			}
		}

		throw new IllegalArgumentException(value + " is not a valid value");
	}

	public static void main(String[] args) throws Exception {
		String s = "start";
		ServerCommandEnum valueOf = ServerCommandEnum.getServerCommandEnum(s);
		System.out.println(valueOf);
	}
}
