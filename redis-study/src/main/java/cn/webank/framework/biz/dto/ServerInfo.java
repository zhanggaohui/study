package cn.webank.framework.biz.dto;

import cn.webank.framework.biz.server.ServerCommandEnum;

public class ServerInfo {

	/**
	 * server地址
	 */
	private String address;

	/**
	 * server shutdown端口
	 */
	private int port = 8005;

	/**
	 * server shutdown命令
	 */
	private String shutdown = "SHUTDOWN";

	/**
	 * server命令
	 */
	private ServerCommandEnum command;

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @param port
	 *            the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * @return the shutdown
	 */
	public String getShutdown() {
		return shutdown;
	}

	/**
	 * @param shutdown
	 *            the shutdown to set
	 */
	public void setShutdown(String shutdown) {
		this.shutdown = shutdown;
	}

	/**
	 * @return the command
	 */
	public ServerCommandEnum getCommand() {
		return command;
	}

	/**
	 * @param command
	 *            the command to set
	 */
	public void setCommand(ServerCommandEnum command) {
		this.command = command;
	}

}
