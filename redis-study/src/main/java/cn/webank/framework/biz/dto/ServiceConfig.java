/**
 * Copyright (C) @2014 Webank Group Holding Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.webank.framework.biz.dto;

import java.lang.reflect.Method;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cn.webank.framework.biz.service.WeBankBaseService;
import cn.webank.framework.dto.BaseDTO;

public class ServiceConfig extends BaseDTO {

	/**
	 * 服务实例
	 */
	private WeBankBaseService service;

	/**
	 * 服务方法
	 */
	@JsonIgnore
	private Method method;

	/**
	 * 请求WeBankMessage子类class name
	 */
	private Class<?> requestMessageClass;

	// private Class<?> reponseMessageClass;

	/**
	 * WeBankMessage子类属性列表
	 */
	private List<String> fieldNames;

	/**
	 * 定义版本范围最小版本版本，最后一位0，表示( ,1 表示[,为了兼容，可以为空，不判断版本
	 */
	private String minVersion;

	/**
	 * 定义版本范围最大版本版本,最后一位0，表示) ,1 表示]，可以为空，不判断版本
	 */
	private String maxVersion;

	// /**
	// * @return the requestMessageClass
	// */
	// public Class<?> getRequestMessageClass() {
	// return requestMessageClass;
	// }
	//
	// /**
	// * @param requestMessageClass
	// * the requestMessageClass to set
	// */
	// public void setRequestMessageClass(Class<?> requestMessageClass) {
	// this.requestMessageClass = requestMessageClass;
	// }
	// /**
	// * @return the reponseMessageClass
	// */
	// public Class<?> getReponseMessageClass() {
	// return reponseMessageClass;
	// }

	/**
	 * @return the requestMessageClass
	 */
	public Class<?> getRequestMessageClass() {
		return requestMessageClass;
	}

	/**
	 * @param requestMessageClass
	 *            the requestMessageClass to set
	 */
	public void setRequestMessageClass(Class<?> requestMessageClass) {
		this.requestMessageClass = requestMessageClass;
	}

	/**
	 * @return the service
	 */
	public WeBankBaseService getService() {
		return service;
	}

	/**
	 * @return the method
	 */
	public Method getMethod() {
		return method;
	}

	/**
	 * @return the fieldNames
	 */
	public List<String> getFieldNames() {
		return fieldNames;
	}

	public ServiceConfig() {

	}

	public ServiceConfig(WeBankBaseService service, Method method, Class<?> requestMessageClass,
			List<String> fieldNames) {
		this(service, method, requestMessageClass, fieldNames, null, null);
	}

	public ServiceConfig(WeBankBaseService service, Method method, Class<?> requestMessageClass,
			List<String> fieldNames, String minVersion, String maxVersion) {
		this.service = service;
		this.method = method;
		this.fieldNames = fieldNames;
		this.requestMessageClass = requestMessageClass;
		this.minVersion = minVersion;
		this.maxVersion = maxVersion;
		// this.reponseMessageClass = reponseMessageClass;
	}

	/**
	 * @return the minVersion
	 */
	public String getMinVersion() {
		return minVersion;
	}

	/**
	 * @param minVersion
	 *            the minVersion to set
	 */
	public void setMinVersion(String minVersion) {
		this.minVersion = minVersion;
	}

	/**
	 * @return the maxVersion
	 */
	public String getMaxVersion() {
		return maxVersion;
	}

	/**
	 * @param maxVersion
	 *            the maxVersion to set
	 */
	public void setMaxVersion(String maxVersion) {
		this.maxVersion = maxVersion;
	}

	public void setService(WeBankBaseService service) {
		this.service = service;
	}

	public void setMethod(Method method) {
		this.method = method;
	}

	public void setFieldNames(List<String> fieldNames) {
		this.fieldNames = fieldNames;
	}

	// public static void main(String[] args) {
	// ServiceConfig ss = new ServiceConfig();
	// ss.setMaxVersion("1.0.0");
	// System.out.println(JsonMapper.nonEmptyMapper().toJson(ss));
	// }
}
