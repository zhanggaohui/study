package cn.webank.framework.data.jedis.pool;

import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedis;
import redis.clients.util.Hashing;
import redis.clients.util.Pool;

public class WeBankShardedJedisPool extends Pool<ShardedJedis> {
	private final static Logger LOG = LoggerFactory
			.getLogger(WeBankShardedJedisPool.class);

	public WeBankShardedJedisPool(final GenericObjectPoolConfig poolConfig,
			List<JedisShardInfo> shards) {
		this(poolConfig, shards, Hashing.MURMUR_HASH);
	}

	public WeBankShardedJedisPool(final GenericObjectPoolConfig poolConfig,
			List<JedisShardInfo> shards, Hashing algo) {
		this(poolConfig, shards, algo, null);
	}

	public WeBankShardedJedisPool(final GenericObjectPoolConfig poolConfig,
			List<JedisShardInfo> shards, Pattern keyTagPattern) {
		this(poolConfig, shards, Hashing.MURMUR_HASH, keyTagPattern);
	}

	public WeBankShardedJedisPool(final GenericObjectPoolConfig poolConfig,
			List<JedisShardInfo> shards, Hashing algo, Pattern keyTagPattern) {
		super(poolConfig, new WeBankShardedJedisFactory(shards, algo,
				keyTagPattern));
	}

	@Override
	public ShardedJedis getResource() {
		ShardedJedis jedis = super.getResource();
		jedis.setDataSource(this);
		return jedis;
	}

	@Override
	public void returnBrokenResource(final ShardedJedis resource) {
		if (resource != null) {
			returnBrokenResourceObject(resource);
		}
	}

	@Override
	public void returnResource(final ShardedJedis resource) {
		if (resource != null) {
			resource.resetState();
			returnResourceObject(resource);
		}
	}

	/**
	 * PoolableObjectFactory custom impl.
	 */
	private static class WeBankShardedJedisFactory implements
			PooledObjectFactory<ShardedJedis> {
		private List<JedisShardInfo> shards;
		private Hashing algo;
		private Pattern keyTagPattern;

		public WeBankShardedJedisFactory(List<JedisShardInfo> shards,
				Hashing algo, Pattern keyTagPattern) {
			this.shards = shards;
			this.algo = algo;
			this.keyTagPattern = keyTagPattern;
		}

		@Override
		public PooledObject<ShardedJedis> makeObject() throws Exception {
			ShardedJedis jedis = new ShardedJedis(shards, algo, keyTagPattern);
			return new DefaultPooledObject<ShardedJedis>(jedis);
		}

		@Override
		public void destroyObject(PooledObject<ShardedJedis> pooledShardedJedis)
				throws Exception {
			final ShardedJedis shardedJedis = pooledShardedJedis.getObject();
			for (Jedis jedis : shardedJedis.getAllShards()) {
				try {
					try {
						jedis.quit();
					} catch (Exception e) {

					}
					jedis.disconnect();
				} catch (Exception e) {

				}
			}
		}

		@Override
		public boolean validateObject(
				PooledObject<ShardedJedis> pooledShardedJedis) {

			// return when one node active
			ShardedJedis jedis = pooledShardedJedis.getObject();
			for (Jedis shard : jedis.getAllShards()) {
				try {
					if (shard.ping().equals("PONG")) {
						return true;
					}
				} catch (Exception ex) {
					// return false;
					LOG.error("validate jedis error", ex);
					continue;
				}
			}
			return false;

		}

		@Override
		public void activateObject(PooledObject<ShardedJedis> p)
				throws Exception {

		}

		@Override
		public void passivateObject(PooledObject<ShardedJedis> p)
				throws Exception {

		}
	}

}
