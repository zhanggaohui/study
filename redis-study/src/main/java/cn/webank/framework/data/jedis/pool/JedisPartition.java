/**
 * Copyright (C) @2014 Webank Group Holding Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.webank.framework.data.jedis.pool;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisSentinelPool;

/**
 * 一组Jedis共享池，实现读写分离
 * 
 * @author xavierdeng
 * @author jonyang
 *
 */
public class JedisPartition {

	/**
	 * 哨兵池
	 */
	private JedisSentinelPool jedisSentinelsPool;

	/**
	 * jedis共享池
	 */
	private WeBankShardedJedisPool shardedJedisPool;

	/**
	 * @return the jedisSentinelsPool
	 */
	public JedisSentinelPool getJedisSentinelsPool() {
		return jedisSentinelsPool;
	}

	/**
	 * @param jedisSentinelsPool
	 *            the jedisSentinelsPool to set
	 */
	public void setJedisSentinelsPool(JedisSentinelPool jedisSentinelsPool) {
		this.jedisSentinelsPool = jedisSentinelsPool;
	}

	public JedisPartition() {

	}

	public JedisPartition(JedisSentinelPool jedisSentinelsPool,
			WeBankShardedJedisPool shardedJedisPool) {
		this.jedisSentinelsPool = jedisSentinelsPool;
		this.shardedJedisPool = shardedJedisPool;
	}

	/**
	 * 获取可读写的jedis对象
	 * 
	 * @return jedis对象
	 */
	public Jedis getReadWriteResource() {
		return jedisSentinelsPool.getResource();
	}

	/**
	 * 获取可读的jedis对象
	 * 
	 * @return jedis对象
	 */
	public WeBankShardedJedis getReadResource() {
		return new WeBankShardedJedis(shardedJedisPool.getResource());
	}

	/**
	 * for test
	 * 
	 * @return the sentinels
	 */
	public JedisSentinelPool getSentinelsPool() {
		return jedisSentinelsPool;
	}

	/**
	 * for test
	 * 
	 * @return the shardedJedisPool
	 */
	public WeBankShardedJedisPool getWeBankShardedJedisPool() {
		return shardedJedisPool;
	}

	/**
	 * @param shardedJedisPool
	 *            the shardedJedisPool to set
	 */
	public void setWeBankShardedJedisPool(
			WeBankShardedJedisPool shardedJedisPool) {
		this.shardedJedisPool = shardedJedisPool;
	}

	public void destroy() {
		if (jedisSentinelsPool != null) {
			jedisSentinelsPool.destroy();
		}

		if (shardedJedisPool != null) {
			shardedJedisPool.destroy();
		}
	}

	/**
	 * 释放资源，将jedis对象返回到pool
	 * 
	 * @param jedis
	 *            Jedis连接对象
	 */
	public void returnReadWriteResource(Jedis jedis) {
		if (jedis != null) {
			jedisSentinelsPool.returnResource(jedis);
		}
	}

	/**
	 * 释放资源，将jedis对象返回到pool
	 * 
	 * @param jedis
	 *            Jedis连接对象
	 */
	public void returnReadResource(WeBankShardedJedis jedis) {
		if (jedis != null) {
			shardedJedisPool.returnResource(jedis.getSharedJedis());
		}
	}

	/**
	 * 释放资源，将jedis对象失效
	 * 
	 * @param jedis
	 *            Jedis连接对象
	 */
	public void returnBrokenReadWriteResource(Jedis jedis) {
		jedisSentinelsPool.returnBrokenResource(jedis);
	}

	/**
	 * 释放资源，将jedis对象失效
	 * 
	 * @param jedis
	 *            Jedis连接对象
	 */
	public void returnBrokenReadResource(WeBankShardedJedis jedis) {
		shardedJedisPool.returnBrokenResource(jedis.getSharedJedis());
	}

	public String toString() {
		return this.jedisSentinelsPool.toString()
				+ this.shardedJedisPool.toString();
	}

}
