package cn.webank.framework.ecif.common.dto;

import cn.webank.framework.dto.BaseDTO;

public class PersonalProduct extends BaseDTO{
	
	private String productAcct;



	/**
	 * 产品类型（A为卡号， C为卡号）
	 * @mbggenerated
	 */
	private String productType;

	/**
	 * 产品编码（）
	 *
	 * @mbggenerated
	 */
	private String productCode;

	/**
	 * 产品状态 （A有效，D无效）
	 *
	 * @mbggenerated
	 */
	private String productStatus;
	
	

	public String getProductAcct() {
		return productAcct;
	}



	public void setProductAcct(String productAcct) {
		this.productAcct = productAcct;
	}



	public String getProductType() {
		return productType;
	}



	public void setProductType(String productType) {
		this.productType = productType;
	}



	public String getProductCode() {
		return productCode;
	}



	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}



	public String getProductStatus() {
		return productStatus;
	}



	public void setProductStatus(String productStatus) {
		this.productStatus = productStatus;
	}



	@Override
	public String toString() {
		return "PersonalProduct [productAcct=" + productAcct + ", productType=" + productType
				+ ", productCode=" + productCode + ", productStatus=" + productStatus + "]";
	}


}
