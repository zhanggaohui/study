package cn.webank.framework.ecif.common.dto;

import cn.webank.framework.dto.BaseDTO;

public class PersonalQQ extends BaseDTO{
	
	/**
	 * 用途
	 */
	private String qqUsage;


  /**
   * qq号
   *
   * @mbggenerated
   */
  private String qq;

  /**
   * openId
   *
   * @mbggenerated
   */
  private String openId;
  
  private String status;

	public String getQqUsage() {
		return qqUsage;
	}

	public void setQqUsage(String qqUsage) {
		this.qqUsage = qqUsage;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "PersonalQQ [qqUsage=" + qqUsage + ", qq=" + qq + ", openId=" + openId + ", status="
				+ status + "]";
	}

	
  
}
