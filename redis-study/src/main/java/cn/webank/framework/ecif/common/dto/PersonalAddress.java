package cn.webank.framework.ecif.common.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;

import cn.webank.framework.dto.BaseDTO;

public class PersonalAddress extends BaseDTO{
	 /**
   * 地址类型
   *
   * @mbggenerated
   */
  private String addressType;

  /**
   * 地址
   *
   * @mbggenerated
   */
  private String address;

  /**
   * 状态
   *
   * @mbggenerated
   */
  private String status;

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("addressType", addressType)
				.append("address", address)
				.append("status", status)
				.toString();
	}
}
