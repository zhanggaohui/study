package cn.webank.framework.ecif.common.util;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import cn.webank.framework.ecif.common.dto.ClientInfo;
import cn.webank.framework.ecif.common.dto.PersonalProduct;
import cn.webank.framework.ecif.common.dto.PersonalTelephone;

/**
 * Created by leaflyhuang on 2015/9/16.
 */
public class EcifClientUtils {

	private final static String RETAIL_CODE = "RETAIL003";

	/**
	 * 判断客户是否零售卡
	 * 
	 * @param clientInfo 客户信息
	 * @return true有零售卡，false无零售卡
	 */
	public static boolean haveRetailCard(ClientInfo clientInfo) {
		if (StringUtils.isBlank(EcifClientUtils.getRetailCard(clientInfo))) {
			return false;
		}
		return true;
	}

	/**
	 * 获取客户零售卡号
	 * 
	 * @param clientInfo 客户信息
	 * @return 零售卡号
	 */
	public static String getRetailCard(ClientInfo clientInfo) {
		return getCard(clientInfo, RETAIL_CODE);
	}

	/**
	 * 获取指定客户卡号
	 * 
	 * @param clientInfo 客户信息
	 * @param productCode 产品代码
	 * @return 卡号
	 */
	public static String getCard(ClientInfo clientInfo, String productCode) {
		if (clientInfo == null || clientInfo.getPersonalProducts() == null) {
			return null;
		}
		String cardNo = "";
		List<PersonalProduct> products = clientInfo.getPersonalProducts();
		for (PersonalProduct product : products) {
			if (product.getProductCode().equals(productCode) && "C".equals(product.getProductType())
					&& "A".equals(product.getProductStatus())) {
				cardNo = product.getProductAcct();
				break;
			}
		}
		return cardNo;
	}

	/**
	 * 获取客户手机号
	 * 
	 * @param clientInfo 客户信息
	 * @param productCode 产品代码
	 * @return 手机号
	 */
	public static String getPhoneNo(ClientInfo clientInfo, String productCode) {
		if (clientInfo == null || clientInfo.getPersonalProducts() == null) {
			return null;
		}
		String phoneNo = "";
		List<PersonalTelephone> telephones = clientInfo.getTelephoneList();
		for (PersonalTelephone telephone : telephones) {
			if (productCode.equals(telephone.getTelephoneUsage()) && "04".equals(telephone.getTelephoneType())
					&& "A".equals(telephone.getStatus())) {
				phoneNo = telephone.getTelephoneNumber();
				break;
			}
		}
		return phoneNo;
	}

	/**
	 * 获取零售手机号
	 * @param clientInfo 客户信息
	 * @return 手机号
	 */
	public static String getRetailPhoneNo(ClientInfo clientInfo) {
		return getPhoneNo(clientInfo, RETAIL_CODE);
	}

}
