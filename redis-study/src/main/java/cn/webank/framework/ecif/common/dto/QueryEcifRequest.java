package cn.webank.framework.ecif.common.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;

import cn.webank.framework.dto.BaseDTO;

/**
 * Created by leaflyhuang on 2015/9/16.
 */
public class QueryEcifRequest extends BaseDTO {

	/**
	 * 查询类型
	 */
	private String queryType;
	/**
	 * 产品帐号
	 */
	private String productAcct;

	/**
	 * Y-需要查客户辅助信息
	 */
	private String needClientAux;

	/**
	 * 客户号
	 */
	private String ecifNo;

	/**
	 * 查询条件QQ号
	 */
	private String qq;

	/**
	 * 微信号
	 */
	private String wechatId;

	/**
	 * QQ openId
	 */
	private String qqOpenId;

	/**
	 * 手机号
	 */
	private String telephoneNumber;
	/**
	 * 微信appId
	 */
	private String wechatAppId;

	/**
	 * 微信openId
	 */
	private String wechatOpenId;

	/**
	 * 微信unionId
	 */
	private String wechatUnionId;

	/**
	 * 第三方账户类型
	 */
	private String coopType;

	/**
	 * 第三方帐号
	 */
	private String coopAccount;

	/**
	 * 需要查询的数据
	 */
	private String needType;

	/**
	 * 客户身份证号
	 */
	private String personalIdentificationNumber;

	public String getQueryType() {
		return queryType;
	}

	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}

	public String getProductAcct() {
		return productAcct;
	}

	public void setProductAcct(String productAcct) {
		this.productAcct = productAcct;
	}

	public String getNeedClientAux() {
		return needClientAux;
	}

	public void setNeedClientAux(String needClientAux) {
		this.needClientAux = needClientAux;
	}

	public String getEcifNo() {
		return ecifNo;
	}

	public void setEcifNo(String ecifNo) {
		this.ecifNo = ecifNo;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getWechatId() {
		return wechatId;
	}

	public void setWechatId(String wechatId) {
		this.wechatId = wechatId;
	}

	public String getQqOpenId() {
		return qqOpenId;
	}

	public void setQqOpenId(String qqOpenId) {
		this.qqOpenId = qqOpenId;
	}

	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public String getWechatAppId() {
		return wechatAppId;
	}

	public void setWechatAppId(String wechatAppId) {
		this.wechatAppId = wechatAppId;
	}

	public String getWechatOpenId() {
		return wechatOpenId;
	}

	public void setWechatOpenId(String wechatOpenId) {
		this.wechatOpenId = wechatOpenId;
	}

	public String getWechatUnionId() {
		return wechatUnionId;
	}

	public void setWechatUnionId(String wechatUnionId) {
		this.wechatUnionId = wechatUnionId;
	}

	public String getCoopType() {
		return coopType;
	}

	public void setCoopType(String coopType) {
		this.coopType = coopType;
	}

	public String getCoopAccount() {
		return coopAccount;
	}

	public void setCoopAccount(String coopAccount) {
		this.coopAccount = coopAccount;
	}

	public String getNeedType() {
		return needType;
	}

	public void setNeedType(String needType) {
		this.needType = needType;
	}

	public String getPersonalIdentificationNumber() {
		return personalIdentificationNumber;
	}

	public void setPersonalIdentificationNumber(String personalIdentificationNumber) {
		this.personalIdentificationNumber = personalIdentificationNumber;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("queryType", queryType).append("productAcct", productAcct)
				.append("needClientAux", needClientAux).append("ecifNo", ecifNo).append("qq", qq)
				.append("wechatId", wechatId).append("qqOpenId", qqOpenId).append("telephoneNumber", telephoneNumber)
				.append("wechatAppId", wechatAppId).append("wechatOpenId", wechatOpenId)
				.append("wechatUnionId", wechatUnionId).append("coopType", coopType).append("coopAccount", coopAccount)
				.append("needType", needType).append("personalIdentificationNumber", personalIdentificationNumber)
				.toString();
	}
}
