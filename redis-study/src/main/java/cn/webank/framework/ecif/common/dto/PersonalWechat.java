package cn.webank.framework.ecif.common.dto;

import cn.webank.framework.dto.BaseDTO;

public class PersonalWechat extends BaseDTO{
	
	private String appId;

  /**
   * 微信号
   *
   * @mbggenerated
   */
  private String wechatId;

  /**
   * 昵称
   *
   * @mbggenerated
   */
  private String nickName;

  /**
   * 性别
   *
   * @mbggenerated
   */
  private String gender;

  /**
   * 城市
   *
   * @mbggenerated
   */
  private String city;

  /**
   * 国家
   * @mbggenerated
   */
  private String country;

  /**
   *省份
   * @mbggenerated
   */
  private String province;

  /**
   * 语言
   *
   * @mbggenerated
   */
  private String language;

  /**
   * 
   * @mbggenerated
   */
  private String openId;

  /**
   * 
   * @mbggenerated
   */
  private String unionId;

  /**
   * 头像
   * @mbggenerated
   */
  private String headImgUrl;

  /**
   * 订阅时间
   *
   * @mbggenerated
   */
  private String subscribeTime;

  /**
   * 状态
   * @mbggenerated
   */
  private String status;
  
  /**
   * 微信用途
   */
  private String wechatUsage;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getWechatId() {
		return wechatId;
	}

	public void setWechatId(String wechatId) {
		this.wechatId = wechatId;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getUnionId() {
		return unionId;
	}

	public void setUnionId(String unionId) {
		this.unionId = unionId;
	}

	public String getHeadImgUrl() {
		return headImgUrl;
	}

	public void setHeadImgUrl(String headImgUrl) {
		this.headImgUrl = headImgUrl;
	}

	public String getSubscribeTime() {
		return subscribeTime;
	}

	public void setSubscribeTime(String subscribeTime) {
		this.subscribeTime = subscribeTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getWechatUsage() {
		return wechatUsage;
	}

	public void setWechatUsage(String wechatUsage) {
		this.wechatUsage = wechatUsage;
	}

	@Override
	public String toString() {
		return "PersonalWechat [appId=" + appId + ", openId=" + openId + ", unionId=" + unionId
				+ ", status=" + status + ", wechatUsage=" + wechatUsage + "]";
	}

}
