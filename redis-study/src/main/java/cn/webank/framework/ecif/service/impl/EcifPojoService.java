package cn.webank.framework.ecif.service.impl;

import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.webank.framework.dto.BizErrors;
import cn.webank.framework.dto.SyncRMBMessage;
import cn.webank.framework.ecif.common.dto.ClientInfo;
import cn.webank.framework.ecif.common.dto.EcifClientContact;
import cn.webank.framework.ecif.common.dto.EcifIndexDTO;
import cn.webank.framework.ecif.common.dto.QueryEcifRequest;
import cn.webank.framework.ecif.common.util.EcifKeys;
import cn.webank.framework.ecif.service.EcifService;
import cn.webank.framework.exception.SysException;
import cn.webank.framework.gns.integration.GNSSao;
import cn.webank.framework.message.integration.RMBSao;
import cn.webank.framework.utils.WeBankContextUtil;
import cn.webank.gns.GNS;

/**
 * Created by leaflyhuang on 2015/9/16.
 */
@Service("cn.webank.framework.ecif.service.EcifService")
public class EcifPojoService implements EcifService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EcifPojoService.class);

	@Autowired
	@Qualifier("cn.webank.framework.message.integration.RMBSao")
	private RMBSao rmbSao;

	@Autowired
	@Qualifier("cn.webank.framework.gns.integration.GNSSao")
	private GNSSao gnsSao;

	@Override
	public EcifIndexDTO openClient(String corporationId, String orgSysId, String bizSeqNo, String consumerSeqNo,
			EcifIndexDTO ecifIndexDTO, BizErrors bizErrors) {
		String dcnNo = gnsSao.getAdminDcnNo();
		SyncRMBMessage<EcifIndexDTO, EcifIndexDTO> message = new SyncRMBMessage<EcifIndexDTO, EcifIndexDTO>() {
		};
		message.setBizSeqNo(bizSeqNo);
		message.setConsumerSeqNo(consumerSeqNo);
		message.setDcnNo(dcnNo);
		message.setOrgSysId(orgSysId);
		message.setRequestObject(ecifIndexDTO);
		message.setScenario("01");
		message.setServiceId("09200001");
		message.setTranTimestamp(new Date());
		message.setUserLang(Locale.CHINESE.toString());
		message.setVersion("1.0.0");
		SyncRMBMessage<EcifIndexDTO, EcifIndexDTO> rtMessage = rmbSao.send(message, 5000, TimeUnit.MILLISECONDS);
		if (rtMessage == null || !"S".equals(rtMessage.getRetStatus())) {
			if (rtMessage.getRetList() != null && rtMessage.getRetList().size() > 0) {
				bizErrors.reject(rtMessage.getRetList().get(0).getCode(), null, rtMessage.getRetList().get(0).getMsg());
			} else {
				throw new SysException("create ecifNo error!");
			}

		}
		return message.getResponseObject();
	}

	@Override
	public ClientInfo queryEcifByEcifNo(String corporationId, String orgSysId, String bizSeqNo, String consumerSeqNo,
			String ecifNo) {
		String dcnNo = gnsSao.getDcnNoByEcifNo(ecifNo);
		QueryEcifRequest request = new QueryEcifRequest();
		request.setEcifNo(ecifNo);
		request.setQueryType(EcifKeys.PERSONAL_QUERY_TYPE_ECIF);
		request.setNeedType("2");
		return queryEcifClient(corporationId, orgSysId, bizSeqNo, consumerSeqNo, dcnNo, request);
	}

	@Override
	public ClientInfo queryEcifByQQOpenId(String corporationId, String orgSysId, String bizSeqNo, String consumerSeqNo,
			String qqOpenId) {
		String dcnNo = gnsSao.getDcnNoByQQOpenId(qqOpenId);
		QueryEcifRequest request = new QueryEcifRequest();
		request.setQqOpenId(qqOpenId);
		request.setQueryType(EcifKeys.PERSONAL_QUERY_TYPE_QQ_OPENID);
		request.setNeedType("2");
		return queryEcifClient(corporationId, orgSysId, bizSeqNo, consumerSeqNo, dcnNo, request);
	}

	@Override
	public ClientInfo queryEcifByWechatUnionId(String corporationId, String orgSysId, String bizSeqNo,
			String consumerSeqNo, String wechatUnionId) {
		String dcnNo = gnsSao.getDcnNoByUnionId(wechatUnionId);
		QueryEcifRequest request = new QueryEcifRequest();
		request.setWechatUnionId(wechatUnionId);
		request.setQueryType(EcifKeys.PERSONAL_QUERY_TYPE_WECHAT_UNIONID);
		request.setNeedType("2");
		return queryEcifClient(corporationId, orgSysId, bizSeqNo, consumerSeqNo, dcnNo, request);
	}

	@Override
	public ClientInfo queryEcifByWechatOpenId(String corporationId, String orgSysId, String bizSeqNo,
			String consumerSeqNo, String wechatOpenId) {
		String dcnNo = gnsSao.getDcnNoByWechatOpenId(wechatOpenId, corporationId);
		QueryEcifRequest request = new QueryEcifRequest();
		request.setWechatOpenId(wechatOpenId);
		request.setQueryType(EcifKeys.PERSONAL_QUERY_TYPE_WECHAT_OPENID);
		request.setNeedType("2");
		return queryEcifClient(corporationId, orgSysId, bizSeqNo, consumerSeqNo, dcnNo, request);
	}

	@Override
	public ClientInfo queryEcifByTelephoeNo(String corporationId, String orgSysId, String bizSeqNo,
			String consumerSeqNo, String telephoenNumber) {
		String dcnNo = "";
		try {
			dcnNo = GNS.queryByMoblie(telephoenNumber);
		} catch (Exception e) {
			throw new SysException("query telephoneNumber error!");
		}
		QueryEcifRequest request = new QueryEcifRequest();
		request.setTelephoneNumber(telephoenNumber);
		request.setQueryType(EcifKeys.PERSONAL_QUERY_TYPE_TELEPHONE);
		request.setNeedType("2");
		return queryEcifClient(corporationId, orgSysId, bizSeqNo, consumerSeqNo, dcnNo, request);
	}

	@Override
	public ClientInfo queryEcifByCoopAccount(String corporationId, String orgSysId, String bizSeqNo,
			String consumerSeqNo, String coopType, String coopAccount) {
		String dcnNo = gnsSao.getDcnNoByCoopAccount(coopType, coopAccount);
		QueryEcifRequest request = new QueryEcifRequest();
		request.setCoopType(coopType);
		request.setCoopAccount(coopAccount);
		request.setQueryType(EcifKeys.PERSONAL_QUERY_TYPE_COOP);
		request.setNeedType("2");
		return queryEcifClient(corporationId, orgSysId, bizSeqNo, consumerSeqNo, dcnNo, request);
	}

	@Override
	public ClientInfo queryEcifByIdentificationNumber(String corporationId, String orgSysId, String bizSeqNo,
			String consumerSeqNo, String identificationType, String identificationNumber) {
		String dcnNo = gnsSao.getDcnNoByEcifId(identificationType, identificationNumber, corporationId);
		QueryEcifRequest request = new QueryEcifRequest();
		request.setPersonalIdentificationNumber(identificationNumber);
		request.setQueryType(EcifKeys.PERSONAL_QUERY_TYPE_IDNO);
		request.setNeedType("2");
		return queryEcifClient(corporationId, orgSysId, bizSeqNo, consumerSeqNo, dcnNo, request);
	}

	@Override
	public ClientInfo queryEcifByCardNo(String corporationId, String orgSysId, String bizSeqNo, String consumerSeqNo,
			String cardNo) {
		String dcnNo = gnsSao.getDcnNoByCardNo(cardNo);
		QueryEcifRequest request = new QueryEcifRequest();
		request.setProductAcct(cardNo);
		request.setQueryType(EcifKeys.PERSONAL_QUERY_TYPE_PRODUCT);
		request.setNeedType("2");
		return queryEcifClient(corporationId, orgSysId, bizSeqNo, consumerSeqNo, dcnNo, request);
	}

	@Override
	public String queryEcifNoById(String corporationId, String orgSysId, String bizSeqNo, String consumerSeqNo,
			String identificationNumber) {
		String dcnNo = gnsSao.getDcnNoByEcifId("01", identificationNumber, corporationId);
		QueryEcifRequest request = new QueryEcifRequest();
		request.setPersonalIdentificationNumber(identificationNumber);
		request.setQueryType(EcifKeys.PERSONAL_QUERY_TYPE_IDNO);
		request.setNeedType("1");
		ClientInfo clientInfo = queryEcifClient(corporationId, orgSysId, bizSeqNo, consumerSeqNo, dcnNo, request);
		return clientInfo == null ? "" : clientInfo.getEcifNo();
	}

	@Override
	public ClientInfo queryEcifClient(String corporationId, String orgSysId, String bizSeqNo, String consumerSeqNo,
			String dcnNo, QueryEcifRequest request) {
		if (StringUtils.isBlank(dcnNo)) {
			throw new SysException("[ecif-sdk] cannot get dcnNo!");
		}
		SyncRMBMessage<QueryEcifRequest, ClientInfo> message = new SyncRMBMessage<QueryEcifRequest, ClientInfo>() {
		};
		message.setBizSeqNo(bizSeqNo);
		message.setConsumerSeqNo(consumerSeqNo);
		message.setDcnNo(dcnNo);
		message.setOrgSysId(orgSysId);
		message.setRequestObject(request);
		message.setScenario("02");
		message.setServiceId("09300001");
		message.setTranTimestamp(new Date());
		message.setUserLang(Locale.CHINESE.toString());
		message.setVersion("1.0.0");
		SyncRMBMessage<QueryEcifRequest, ClientInfo> rtMessage = rmbSao.send(message, 5000, TimeUnit.MILLISECONDS);
		if (rtMessage == null || !"S".equals(rtMessage.getRetStatus())) {
			LOGGER.warn(WeBankContextUtil.getBizSeqNo() + "queryEcif error");
			return new ClientInfo();
		}
		ClientInfo clientInfo = rtMessage.getResponseObject();
		clientInfo.setDcnNo(dcnNo);
		return clientInfo;
	}

	@Override
	public void addEcifClientContact(String corporationId, String orgSysId, String bizSeqNo, String consumerSeqNo,
			String dcnNo, EcifClientContact ecifClientContact) {
		if (StringUtils.isBlank(dcnNo)) {
			throw new SysException("[ecif-sdk] dcn is null!");
		}
		SyncRMBMessage<EcifClientContact, EcifClientContact> message = new SyncRMBMessage<EcifClientContact, EcifClientContact>() {
		};
		message.setBizSeqNo(bizSeqNo);
		message.setConsumerSeqNo(consumerSeqNo);
		message.setDcnNo(dcnNo);
		message.setOrgSysId(orgSysId);
		message.setRequestObject(ecifClientContact);
		message.setScenario("01");
		message.setServiceId("09200022");
		message.setTranTimestamp(new Date());
		message.setUserLang(Locale.CHINESE.toString());
		message.setVersion("1.0.0");
		SyncRMBMessage<EcifClientContact, EcifClientContact> rtMessage = rmbSao.send(message, 5000,
				TimeUnit.MILLISECONDS);
		if (rtMessage == null || !"S".equals(rtMessage.getRetStatus())) {
			throw new SysException("[ecif-sdk] add ecif client contact error!");
		}
	}

	public RMBSao getRmbSao() {
		return rmbSao;
	}

	public GNSSao getGnsSao() {
		return gnsSao;
	}

	public void setRmbSao(RMBSao rmbSao) {
		this.rmbSao = rmbSao;
	}

	public void setGnsSao(GNSSao gnsSao) {
		this.gnsSao = gnsSao;
	}
}
