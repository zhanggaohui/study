package cn.webank.framework.ecif.common.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;

import cn.webank.framework.dto.BaseDTO;

/**
 *
 */
public class EcifIndexDTO extends BaseDTO {
	private String personalName;
	private String personalIdentificationNumber;
	private String personalIdentificationType;
	private String ecifNo;
	private String dcnNo;
	private boolean isOldEcif;
	private String source;
	private String qqOpenId;
	private String wechatOpenId;
	private String wechatUnionId;

	public EcifIndexDTO() {
	}

	public EcifIndexDTO(String personalName, String personalIdentificationNumber, String personalIdentificationType, String source) {
		this.personalName = personalName;
		this.personalIdentificationNumber = personalIdentificationNumber;
		this.personalIdentificationType = personalIdentificationType;
		this.source = source;
	}

	public String getPersonalName() {
		return personalName;
	}

	public void setPersonalName(String personalName) {
		this.personalName = personalName;
	}

	public String getPersonalIdentificationNumber() {
		return personalIdentificationNumber;
	}

	public void setPersonalIdentificationNumber(String personalIdentificationNumber) {
		this.personalIdentificationNumber = personalIdentificationNumber;
	}

	public String getPersonalIdentificationType() {
		return personalIdentificationType;
	}

	public void setPersonalIdentificationType(String personalIdentificationType) {
		this.personalIdentificationType = personalIdentificationType;
	}

	public String getEcifNo() {
		return ecifNo;
	}

	public void setEcifNo(String ecifNo) {
		this.ecifNo = ecifNo;
	}

	public String getDcnNo() {
		return dcnNo;
	}

	public void setDcnNo(String dcnNo) {
		this.dcnNo = dcnNo;
	}

	public boolean isOldEcif() {
		return isOldEcif;
	}

	public void setIsOldEcif(boolean isOldEcif) {
		this.isOldEcif = isOldEcif;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getQqOpenId() {
		return qqOpenId;
	}

	public void setQqOpenId(String qqOpenId) {
		this.qqOpenId = qqOpenId;
	}

	public String getWechatOpenId() {
		return wechatOpenId;
	}

	public void setWechatOpenId(String wechatOpenId) {
		this.wechatOpenId = wechatOpenId;
	}

	public String getWechatUnionId() {
		return wechatUnionId;
	}

	public void setWechatUnionId(String wechatUnionId) {
		this.wechatUnionId = wechatUnionId;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("personalName", personalName)
				.append("personalIdentificationNumber", personalIdentificationNumber)
				.append("personalIdentificationType", personalIdentificationType)
				.append("ecifNo", ecifNo)
				.append("dcnNo", dcnNo)
				.append("isOldEcif", isOldEcif)
				.append("source", source)
				.append("qqOpenId", qqOpenId)
				.append("wechatOpenId", wechatOpenId)
				.append("wechatUnionId", wechatUnionId)
				.toString();
	}
}
