package cn.webank.framework.ecif.common.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;

import cn.webank.framework.dto.BaseDTO;

/**
 * Created by leaflyhuang on 2015/9/16.
 */
public class EcifClientContact extends BaseDTO{
    private String ecifNo;
    private String usage;
    private String bindType;
    private String wechatUnionId;
    private String wechatOpenId;
    private String appId;
    private String qqOpenId;
    private String telephoneNumber;
    private String coopType;
    private String coopAccount;

    public String getEcifNo() {
        return ecifNo;
    }

    public void setEcifNo(String ecifNo) {
        this.ecifNo = ecifNo;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    public String getBindType() {
        return bindType;
    }

    public void setBindType(String bindType) {
        this.bindType = bindType;
    }

    public String getWechatUnionId() {
        return wechatUnionId;
    }

    public void setWechatUnionId(String wechatUnionId) {
        this.wechatUnionId = wechatUnionId;
    }

    public String getWechatOpenId() {
        return wechatOpenId;
    }

    public void setWechatOpenId(String wechatOpenId) {
        this.wechatOpenId = wechatOpenId;
    }

    public String getQqOpenId() {
        return qqOpenId;
    }

    public void setQqOpenId(String qqOpenId) {
        this.qqOpenId = qqOpenId;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getCoopType() {
        return coopType;
    }

    public void setCoopType(String coopType) {
        this.coopType = coopType;
    }

    public String getCoopAccount() {
        return coopAccount;
    }

    public void setCoopAccount(String coopAccount) {
        this.coopAccount = coopAccount;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("ecifNo", ecifNo)
                .append("usage", usage)
                .append("bindType", bindType)
                .append("wechatUnionId", wechatUnionId)
                .append("wechatOpenId", wechatOpenId)
                .append("appId", appId)
                .append("qqOpenId", qqOpenId)
                .append("telephoneNumber", telephoneNumber)
                .append("coopType", coopType)
                .append("coopAccount", coopAccount)
                .toString();
    }
}
