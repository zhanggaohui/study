package cn.webank.framework.ecif.common.util;

/**
 * Created by leaflyhuang on 2015/9/16.
 */
public class EcifKeys {

    /**
     * 查询客户信息-根据ecif号查询
     */
    public static final String PERSONAL_QUERY_TYPE_ECIF = "001";

    /**
     * 查询客户信息-根据QQ号查询
     */
    public static final String PERSONAL_QUERY_TYPE_QQ = "002";

    /**
     * 查询客户信息-根据QQ的openId查询
     */
    public static final String PERSONAL_QUERY_TYPE_QQ_OPENID = "005";

    /**
     * 查询客户信息-根据QQ的openId查询
     */
    public static final String PERSONAL_QUERY_TYPE_PRODUCT = "004";


    /**
     * 查询客户信息-根据手机号码
     */
    public static final String PERSONAL_QUERY_TYPE_TELEPHONE = "006";

    /**
     * 查询客户信息-根据微信openId
     */
    public static final String PERSONAL_QUERY_TYPE_WECHAT_OPENID = "007";

    /**
     * 查询客户信息-根据微信unionId
     */
    public static final String PERSONAL_QUERY_TYPE_WECHAT_UNIONID = "008";

    /**
     * 查询客户信息-根据微信unionId
     */
    public static final String PERSONAL_QUERY_TYPE_COOP = "009";

    /**
     * 查询客户信息-根据身份证号查询
     */
    public static final String PERSONAL_QUERY_TYPE_IDNO = "010";
}