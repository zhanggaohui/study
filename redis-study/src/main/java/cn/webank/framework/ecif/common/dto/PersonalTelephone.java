package cn.webank.framework.ecif.common.dto;

import cn.webank.framework.dto.BaseDTO;

public class PersonalTelephone extends BaseDTO{
	/**
	 * 电话用途
	 */
	private String telephoneUsage;


	/**
	 * 
	 *电话类型 （04：手机）
	 * @mbggenerated
	 */
	private String telephoneType;

	/**
	 * 国际区号
	 *
	 * @mbggenerated
	 */
	private String iddCode;

	/**
	 * 区号
	 *
	 * @mbggenerated
	 */
	private String dddCode;

	/**
	 * 电话号码
	 *
	 * @mbggenerated
	 */
	private String telephoneNumber;

	/**
	 * 分机号
	 *
	 * @mbggenerated
	 */
	private String extensionNumber;

	/**
	 * 状态
	 *
	 * @mbggenerated
	 */
	private String status;

	public String getTelephoneUsage() {
		return telephoneUsage;
	}

	public void setTelephoneUsage(String telephoneUsage) {
		this.telephoneUsage = telephoneUsage;
	}

	public String getTelephoneType() {
		return telephoneType;
	}

	public void setTelephoneType(String telephoneType) {
		this.telephoneType = telephoneType;
	}

	public String getIddCode() {
		return iddCode;
	}

	public void setIddCode(String iddCode) {
		this.iddCode = iddCode;
	}

	public String getDddCode() {
		return dddCode;
	}

	public void setDddCode(String dddCode) {
		this.dddCode = dddCode;
	}

	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public String getExtensionNumber() {
		return extensionNumber;
	}

	public void setExtensionNumber(String extensionNumber) {
		this.extensionNumber = extensionNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "PersonalTelephone [telephoneUsage=" + telephoneUsage + ", telephoneType="
				+ telephoneType + ", iddCode=" + iddCode + ", dddCode=" + dddCode + ", telephoneNumber="
				+ telephoneNumber + ", extensionNumber=" + extensionNumber + ", status=" + status + "]";
	}
	
	

}
