package cn.webank.framework.ecif.service;

import cn.webank.framework.dto.BizErrors;
import cn.webank.framework.ecif.common.dto.ClientInfo;
import cn.webank.framework.ecif.common.dto.EcifClientContact;
import cn.webank.framework.ecif.common.dto.EcifIndexDTO;
import cn.webank.framework.ecif.common.dto.QueryEcifRequest;

/**
 * Created by leaflyhuang on 2015/9/16.
 */
public interface EcifService {

	/**
	 * 开立ecif客户号
	 * @param corporationId 法人ID
	 * @param orgSysId 系统ID
	 * @param bizSeqNo 业务流水号
	 * @param consumerSeqNo 系统流水号
	 * @param ecifIndexDTO ecif索引信息
	 * @param bizErrors 业务异常
	 * @return ecif索引信息
	 */
	public EcifIndexDTO openClient(String corporationId, String orgSysId, String bizSeqNo, String consumerSeqNo,
			EcifIndexDTO ecifIndexDTO, BizErrors bizErrors);

	/**
	 * 根据ecif号查客户信息
	 * @param corporationId 法人ID
	 * @param orgSysId 系统ID
	 * @param bizSeqNo 业务流水号
	 * @param consumerSeqNo 系统流水号 
	 * @param ecifNo ecif号
	 * @return 客户信息
	 */
	public ClientInfo queryEcifByEcifNo(String corporationId, String orgSysId, String bizSeqNo, String consumerSeqNo,
			String ecifNo);

	/**
	 * 根据qqopenid号查客户信息
	 *
	 * @param corporationId 法人ID
	 * @param orgSysId 系统ID
	 * @param bizSeqNo 业务流水号
	 * @param consumerSeqNo 系统流水号 
	 * @param qqOpenId qq open id
	 * @return 客户信息
	 */
	public ClientInfo queryEcifByQQOpenId(String corporationId, String orgSysId, String bizSeqNo, String consumerSeqNo,
			String qqOpenId);

	/**
	 * 根据微信UnionId号查客户信息
	 *
	 * @param corporationId 法人ID
	 * @param orgSysId 系统ID
	 * @param bizSeqNo 业务流水号
	 * @param consumerSeqNo 系统流水号 
	 * @param wechatUnionId 微信union id
	 * @return 客户信息
	 */
	public ClientInfo queryEcifByWechatUnionId(String corporationId, String orgSysId, String bizSeqNo,
			String consumerSeqNo, String wechatUnionId);

	/**
	 * 根据微信openId号查客户信息
	 *
	 * @param corporationId 法人ID
	 * @param orgSysId 系统ID
	 * @param bizSeqNo 业务流水号
	 * @param consumerSeqNo 系统流水号 
	 * @param wechatOpenId 微信 open id
	 * @return 客户信息
	 */
	public ClientInfo queryEcifByWechatOpenId(String corporationId, String orgSysId, String bizSeqNo,
			String consumerSeqNo, String wechatOpenId);

	/**
	 * 根据手机号查客户信息
	 *
	 * @param corporationId 法人ID
	 * @param orgSysId 系统ID
	 * @param bizSeqNo 业务流水号
	 * @param consumerSeqNo 系统流水号 
	 * @param telephoenNumber 电话号码
	 * @return 客户信息
	 */
	public ClientInfo queryEcifByTelephoeNo(String corporationId, String orgSysId, String bizSeqNo,
			String consumerSeqNo, String telephoenNumber);

	/**
	 * 根据第三方帐号号查客户信息
	 *
	 * @param corporationId 法人ID
	 * @param orgSysId 系统ID
	 * @param bizSeqNo 业务流水号
	 * @param consumerSeqNo 系统流水号 
	 * @param coopType 合作方类型，与gns保持一致
	 * @param coopAccount 合作方账号，与gns保持一致
	 * @return 客户信息
	 */
	public ClientInfo queryEcifByCoopAccount(String corporationId, String orgSysId, String bizSeqNo,
			String consumerSeqNo, String coopType, String coopAccount);

	/**
	 * 根据身份证号号查客户信息
	 * 
	 * @param corporationId 法人ID
	 * @param orgSysId 系统ID
	 * @param bizSeqNo 业务流水号
	 * @param consumerSeqNo 系统流水号
	 * @param identificationType 证件类型，与gns保持一致
	 * @param identificationNumber 证件号
	 * @return 客户信息
	 */
	public ClientInfo queryEcifByIdentificationNumber(String corporationId, String orgSysId, String bizSeqNo,
			String consumerSeqNo, String identificationType, String identificationNumber);

	/**
	 * 根据卡号查客户信息
	 * 
	 * @param corporationId 法人ID
	 * @param orgSysId 系统ID
	 * @param bizSeqNo 业务流水号
	 * @param consumerSeqNo 系统流水号
	 * @param cardNo 银行卡号
	 * @return 客户信息
	 */
	public ClientInfo queryEcifByCardNo(String corporationId, String orgSysId, String bizSeqNo, String consumerSeqNo,
			String cardNo);

	/**
	 * 根据e身份证号查eicf号
	 *
	 * @param corporationId 法人ID
	 * @param orgSysId 系统ID
	 * @param bizSeqNo 业务流水号
	 * @param consumerSeqNo 系统流水号
	 * @param identificationNumber 身份证ID
	 * @return ecif号
	 */
	public String queryEcifNoById(String corporationId, String orgSysId, String bizSeqNo, String consumerSeqNo,
			String identificationNumber);

	/**
	 * 查询客户信息
	 * 
	 * @param corporationId 法人ID
	 * @param orgSysId 系统ID
	 * @param bizSeqNo 业务流水号
	 * @param consumerSeqNo 系统流水号
	 * @param dcnNo  dcn号
	 * @param request 查询请求对象
	 * @return 客户信息
	 */
	public ClientInfo queryEcifClient(String corporationId, String orgSysId, String bizSeqNo, String consumerSeqNo,
			String dcnNo, QueryEcifRequest request);

	/**
	 * 新增客户联系信息：手机号、qqOpenId、微信openId、微信UnionId、第三方帐号
	 * 
	 * @param corporationId 法人ID
	 * @param orgSysId 系统ID
	 * @param bizSeqNo 业务流水号
	 * @param consumerSeqNo 系统流水号
	 * @param dcnNo dcn号
	 * @param ecifClientContact 客户联系信息对象
	 */
	public void addEcifClientContact(String corporationId, String orgSysId, String bizSeqNo, String consumerSeqNo,
			String dcnNo, EcifClientContact ecifClientContact);

}
