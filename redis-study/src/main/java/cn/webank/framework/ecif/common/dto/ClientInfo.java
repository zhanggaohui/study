package cn.webank.framework.ecif.common.dto;

import java.util.ArrayList;
import java.util.List;

import cn.webank.framework.dto.BaseDTO;

/**
 * 客户信息 description:
 *
 * @author leaflyhuang
 * @version 1.0
 *
 */
public class ClientInfo extends BaseDTO {

	/**
	 * 客户号
	 */
	private String ecifNo;

	/**
	 * 姓名
	 */
	private String personalName;

	/**
	 * 证件号码
	 */
	private String personalIdentificationNumber;

	/**
	 * 证件类型（01：身份证）
	 */
	private String personalIdentificationType;

	/**
	 * 出生年月日
	 */
	private String birthDay;

	/**
	 * 性别
	 */
	private String gender;

	/**
	 * dcn号
	 */
	private String dcnNo;

	/**
	 * 客户产品（卡号-帐号）
	 */
	private List<PersonalProduct> personalProducts;

	/**
	 * 微信列表
	 */
	private List<PersonalWechat> wechatList = new ArrayList<PersonalWechat>();

	/**
	 * QQ列表
	 */
	private List<PersonalQQ> qqList = new ArrayList<PersonalQQ>();

	/**
	 * 邮箱列表
	 */
	private List<PersonalEmail> emailList = new ArrayList<PersonalEmail>();

	/**
	 * 电话列表
	 */
	private List<PersonalTelephone> telephoneList = new ArrayList<PersonalTelephone>();

	/**
	 * 地址列表
	 */
	private List<PersonalAddress> addressList = new ArrayList<PersonalAddress>();

	public String getEcifNo() {
		return ecifNo;
	}

	public void setEcifNo(String ecifNo) {
		this.ecifNo = ecifNo;
	}

	public String getPersonalName() {
		return personalName;
	}

	public void setPersonalName(String personalName) {
		this.personalName = personalName;
	}

	public String getPersonalIdentificationNumber() {
		return personalIdentificationNumber;
	}

	public void setPersonalIdentificationNumber(String personalIdentificationNumber) {
		this.personalIdentificationNumber = personalIdentificationNumber;
	}

	public String getPersonalIdentificationType() {
		return personalIdentificationType;
	}

	public void setPersonalIdentificationType(String personalIdentificationType) {
		this.personalIdentificationType = personalIdentificationType;
	}

	public String getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public List<PersonalProduct> getPersonalProducts() {
		return personalProducts;
	}

	public void setPersonalProducts(List<PersonalProduct> personalProducts) {
		this.personalProducts = personalProducts;
	}

	public List<PersonalWechat> getWechatList() {
		return wechatList;
	}

	public void setWechatList(List<PersonalWechat> wechatList) {
		this.wechatList = wechatList;
	}

	public List<PersonalQQ> getQqList() {
		return qqList;
	}

	public void setQqList(List<PersonalQQ> qqList) {
		this.qqList = qqList;
	}

	public List<PersonalEmail> getEmailList() {
		return emailList;
	}

	public void setEmailList(List<PersonalEmail> emailList) {
		this.emailList = emailList;
	}

	public List<PersonalTelephone> getTelephoneList() {
		return telephoneList;
	}

	public void setTelephoneList(List<PersonalTelephone> telephoneList) {
		this.telephoneList = telephoneList;
	}

	public List<PersonalAddress> getAddressList() {
		return addressList;
	}

	public void setAddressList(List<PersonalAddress> addressList) {
		this.addressList = addressList;
	}

	public String getDcnNo() {
		return dcnNo;
	}

	public void setDcnNo(String dcnNo) {
		this.dcnNo = dcnNo;
	}

	@Override
	public String toString() {
		return "ClientInfo{" + "ecifNo='" + ecifNo + '\'' + ", personalName='" + personalName + '\''
				+ ", personalIdentificationNumber='" + personalIdentificationNumber + '\''
				+ ", personalIdentificationType='" + personalIdentificationType + '\'' + ", birthDay='" + birthDay
				+ '\'' + ", gender='" + gender + '\'' + ", dcnNo='" + dcnNo + '\'' + ", personalProducts="
				+ personalProducts + ", wechatList=" + wechatList + ", qqList=" + qqList + ", emailList=" + emailList
				+ ", telephoneList=" + telephoneList + ", addressList=" + addressList + '}';
	}

}
