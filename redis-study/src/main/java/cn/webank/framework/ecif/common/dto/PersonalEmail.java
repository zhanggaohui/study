package cn.webank.framework.ecif.common.dto;

import cn.webank.framework.dto.BaseDTO;

public class PersonalEmail extends BaseDTO{
	/**
	 *邮箱地址
	 *
	 * @mbggenerated
	 */
	private String emailAddress;

	/**
	 * 状态
	 * @mbggenerated
	 */
	private String status;
	
	/**
	 * 邮箱用途
	 */
	private String emailUsage;

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getEmailUsage() {
		return emailUsage;
	}

	public void setEmailUsage(String emailUsage) {
		this.emailUsage = emailUsage;
	}

	@Override
	public String toString() {
		return "PersonalEmail [emailAddress=" + emailAddress + ", status=" + status + ", emailUsage="
				+ emailUsage + "]";
	}
	
	
	

}
