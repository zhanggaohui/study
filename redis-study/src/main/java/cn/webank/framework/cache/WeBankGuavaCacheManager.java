/**
 * Copyright (C) @2014 Webank Group Holding Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.webank.framework.cache;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.guava.GuavaCache;

import com.google.common.cache.CacheBuilder;

public class WeBankGuavaCacheManager implements CacheManager {
	private final Map<String, Cache> cachMap = new ConcurrentHashMap<String, Cache>(
			5);

	public WeBankGuavaCacheManager(Map<String, String> configMap) {
		if (configMap != null) {
			for (Map.Entry<String, String> obj : configMap.entrySet()) {
				CacheBuilder<Object, Object> cacheBuilder = CacheBuilder
						.from(obj.getValue());
				com.google.common.cache.Cache<Object, Object> cache = cacheBuilder
						.build();
				GuavaCache guavaCache = new GuavaCache(obj.getKey(), cache,
						true);
				this.cachMap.put(obj.getKey(), guavaCache);
			}
		}

	}

	@Override
	public Cache getCache(String name) {
		return this.cachMap.get(name);
	}

	@Override
	public Collection<String> getCacheNames() {
		return Collections.unmodifiableSet(this.cachMap.keySet());
	}

}
