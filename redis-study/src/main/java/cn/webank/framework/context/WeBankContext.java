/**
 * Copyright (C) @2014 Webank Group Holding Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.webank.framework.context;

import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class WeBankContext {
	/**
	 * 业务流水号
	 */
	private String bizSeqNo;

	/**
	 * 区域
	 */
	private Locale locale = Locale.CHINESE;

	/**
	 * dcn号
	 */
	private String dcnNo;
	/**
	 * 第一次调用的系统id
	 */
	private String orgSysId;
	/**
	 * 上一个调用者的系统id
	 */
	private String lastConsumerId;

	/**
	 * 上一个调用者的系统流水号
	 */
	private String lastConsumerSeqNo;

	/**
	 * 反冲流水号
	 */
	private String reversalSeqNo;

	/**
	 * 本系统ID
	 */
	private String sysId;

	/**
	 * appId
	 */
	private String appId;

	/**
	 * 业务对象
	 */
	private Map<String, Object> bizObj = new ConcurrentHashMap<String, Object>();

	public <E> void setBizValue(String key, E value) {
		this.bizObj.put(key, value);
	}

	@SuppressWarnings("unchecked")
	public <E> E getBizValue(String key) {
		return (E) this.bizObj.get(key);
	}

	/**
	 * @return the sysId
	 */
	public String getSysId() {
		return sysId;
	}

	/**
	 * @param sysId
	 *            the sysId to set
	 */
	public void setSysId(String sysId) {
		this.sysId = sysId;
	}

	/**
	 * @return the lastConsumerId
	 */
	public String getLastConsumerId() {
		return lastConsumerId;
	}

	/**
	 * @param lastConsumerId
	 *            the lastConsumerId to set
	 */
	public void setLastConsumerId(String lastConsumerId) {
		this.lastConsumerId = lastConsumerId;
	}

	/**
	 * @return the lastSysSeqNo
	 */
	public String getLastConsumerSeqNo() {
		return lastConsumerSeqNo;
	}

	/**
	 * @param lastConsumerSeqNo
	 *            the lastConsumerSeqNo to set
	 */
	public void setLastConsumerSeqNo(String lastConsumerSeqNo) {
		this.lastConsumerSeqNo = lastConsumerSeqNo;
	}

	/**
	 * @return the dcnNo
	 */
	public String getDcnNo() {
		return dcnNo;
	}

	/**
	 * @param dcnNo
	 *            the dcnNo to set
	 */
	public void setDcnNo(String dcnNo) {
		this.dcnNo = dcnNo;
	}

	/**
	 * @return the orgSysId
	 */
	public String getOrgSysId() {
		return orgSysId;
	}

	/**
	 * @param orgSysId
	 *            the orgSysId to set
	 */
	public void setOrgSysId(String orgSysId) {
		this.orgSysId = orgSysId;
	}

	/**
	 * @return the bizSeqNo
	 */
	public String getBizSeqNo() {
		return bizSeqNo;
	}

	/**
	 * @return the locale
	 */
	public Locale getLocale() {
		return locale;
	}

	/**
	 * @param locale
	 *            the locale to set
	 */
	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	/**
	 * @param bizSeqNo
	 *            the bizSeqNo to set
	 */
	public void setBizSeqNo(String bizSeqNo) {
		this.bizSeqNo = bizSeqNo;
	}

	/**
	 * @return the reversalSeqNo
	 */
	public String getReversalSeqNo() {
		return reversalSeqNo;
	}

	/**
	 * @param reversalSeqNo
	 *            the reversalSeqNo to set
	 */
	public void setReversalSeqNo(String reversalSeqNo) {
		this.reversalSeqNo = reversalSeqNo;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

}
