package cn.webank.framework.concurrent.integration.dao.impl;

import java.sql.SQLException;
import java.util.Map;

import org.springframework.stereotype.Repository;

import cn.webank.framework.concurrent.integration.dao.DistributedLockDAO;
import cn.webank.framework.concurrent.model.LockControl;
import cn.webank.framework.integration.dao.session.WeBankSqlSession;
import cn.webank.framework.integration.dao.support.AbstractSimpleDao;

@Repository("cn.webank.framework.concurrent.integration.dao.DistributedLockDAO")
public class DistributedLockMyBatisDAO extends AbstractSimpleDao implements DistributedLockDAO {

	@Override
	public LockControl queryOldValueByModuleId(String moduleId) throws SQLException {
		try (WeBankSqlSession session = getSession()) {
			DistributedLockDAO mapper = (DistributedLockDAO) session.getMapper(DistributedLockDAO.class);
			return mapper.queryOldValueByModuleId(moduleId);
		}
	}

	@Override
	public int updateNewValue(Map<String, ?> paramMap) throws SQLException {
		try (WeBankSqlSession session = getSession()) {
			DistributedLockDAO mapper = (DistributedLockDAO) session.getMapper(DistributedLockDAO.class);
			return mapper.updateNewValue(paramMap);
		}
	}

	@Override
	public void insertValue(LockControl lock) throws SQLException {
		try (WeBankSqlSession session = getSession()) {
			DistributedLockDAO mapper = (DistributedLockDAO) session.getMapper(DistributedLockDAO.class);
			mapper.insertValue(lock);
		}

	}

	@Override
	public void deleteValueByModuleId(String moduleId) throws SQLException {
		try (WeBankSqlSession session = getSession()) {
			DistributedLockDAO mapper = (DistributedLockDAO) session.getMapper(DistributedLockDAO.class);
			mapper.deleteValueByModuleId(moduleId);
		}

	}

	@Override
	public LockControl lockOldValueByModuleId(String moduleId) throws SQLException {
		try (WeBankSqlSession session = getSession()) {
			DistributedLockDAO mapper = (DistributedLockDAO) session.getMapper(DistributedLockDAO.class);
			return mapper.lockOldValueByModuleId(moduleId);
		}
	}

}
