package cn.webank.framework.concurrent.integration.dao;

import java.sql.SQLException;
import java.util.Map;

import cn.webank.framework.concurrent.model.LockControl;

public interface DistributedLockDAO {
	
	public LockControl queryOldValueByModuleId(String moduleId) throws SQLException;

	public int updateNewValue(Map<String, ?> paramMap) throws SQLException;

	public void insertValue(LockControl seq) throws SQLException;

	public void deleteValueByModuleId(String moduleId) throws SQLException;

	public LockControl lockOldValueByModuleId(String moduleId) throws SQLException;

}
