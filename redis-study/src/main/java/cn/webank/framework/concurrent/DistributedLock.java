package cn.webank.framework.concurrent;

import java.util.concurrent.TimeUnit;

public interface DistributedLock {

	/**
	 * 尝试获取锁
	 * @param tryLockTimeout 在此期间一直尝试获取锁
	 * @param tryLockTimeUnit 时间单位
	 * @return 获取锁结果，true获取到锁，false没有获取到锁
	 */
	public boolean tryLock(long tryLockTimeout, TimeUnit tryLockTimeUnit);
	
	public void unlock();

}
