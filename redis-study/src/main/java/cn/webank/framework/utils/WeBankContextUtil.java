/**
 * Copyright (C) @2014 Webank Group Holding Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.webank.framework.utils;

import java.util.Locale;

import cn.webank.framework.context.WeBankContext;
import cn.webank.framework.context.WeBankContextThreadLocal;

/**
 * 线程上下文工具类
 * 
 * @author jonyang
 *
 */
public class WeBankContextUtil {
	/**
	 * 获取当前线程绑定的反冲流水号
	 * 
	 * @return 反冲流水号，如果不存在则返回null
	 */
	public static String getReversalSeqNo() {
		WeBankContext weBankContext = WeBankContextThreadLocal.get();
		String reversalSeqNo = null;
		if (weBankContext != null) {
			reversalSeqNo = weBankContext.getReversalSeqNo();
		}

		return reversalSeqNo;
	}

	/**
	 * 设置反冲流水号
	 * 
	 * @param reversalSeqNo
	 *            反冲流水号
	 * 
	 */
	public static void setReversalSeqNo(String reversalSeqNo) {
		WeBankContext weBankContext = WeBankContextThreadLocal.get();
		if (weBankContext == null) {
			weBankContext = new WeBankContext();
			setContext(weBankContext);
		}

		weBankContext.setReversalSeqNo(reversalSeqNo);
	}

	/**
	 * 获取当前线程绑定的业务流水号
	 * 
	 * @return 业务流水号，如果不存在则返回null
	 */
	public static String getBizSeqNo() {
		WeBankContext weBankContext = WeBankContextThreadLocal.get();
		String bizSeqNo = null;
		if (weBankContext != null) {
			bizSeqNo = weBankContext.getBizSeqNo();
		}

		return bizSeqNo;
	}

	/**
	 * 设置业务流水号
	 * 
	 * @param bizSeqNo
	 *            业务流水号
	 * 
	 */
	public static void setBizSeqNo(String bizSeqNo) {
		WeBankContext weBankContext = WeBankContextThreadLocal.get();

		if (weBankContext == null) {
			weBankContext = new WeBankContext();
			setContext(weBankContext);
		}
		weBankContext.setBizSeqNo(bizSeqNo);

	}

	/**
	 * 获取当前的dcn号
	 * 
	 * @return dcn号
	 */
	public static String getDcnNo() {
		WeBankContext weBankContext = WeBankContextThreadLocal.get();
		String dcnNo = null;
		if (weBankContext != null) {
			dcnNo = weBankContext.getDcnNo();
		}

		return dcnNo;
	}

	/**
	 * 获取原始调用者的系统id
	 * 
	 * @return 原始调用者的系统id
	 */
	public static String getOrgSysId() {
		WeBankContext weBankContext = WeBankContextThreadLocal.get();
		String orgSysId = null;
		if (weBankContext != null) {
			orgSysId = weBankContext.getOrgSysId();
		}

		return orgSysId;
	}

	/**
	 * 获取本系统id
	 * 
	 * @return 本系统id
	 */
	public static String getSysId() {
		WeBankContext weBankContext = WeBankContextThreadLocal.get();
		String sysId = null;
		if (weBankContext != null) {
			sysId = weBankContext.getSysId();
		}

		return sysId;
	}

	/**
	 * 获取上一个调用者的系统id
	 * 
	 * @return 上一个调用者的系统id
	 */
	public static String getLastConsumerId() {
		WeBankContext weBankContext = WeBankContextThreadLocal.get();
		String consumerId = null;
		if (weBankContext != null) {
			consumerId = weBankContext.getLastConsumerId();
		}

		return consumerId;
	}

	/**
	 * 获取上一个调用者的系统流水号
	 * 
	 * @return 上一个调用者的系统流水号
	 */
	public static String getLastConsumerSeqNo() {
		WeBankContext weBankContext = WeBankContextThreadLocal.get();
		String consumerId = null;
		if (weBankContext != null) {
			consumerId = weBankContext.getLastConsumerSeqNo();
		}

		return consumerId;
	}

	/**
	 * 获取当前区域
	 * 
	 * @return 当前区域
	 */
	public static Locale getLocale() {
		WeBankContext weBankContext = WeBankContextThreadLocal.get();
		Locale locale = Locale.CHINESE;
		if (weBankContext != null) {
			locale = weBankContext.getLocale();
		}

		return locale;
	}

	/**
	 * 为当前线程设置上下文
	 * 
	 * @param weBankContext
	 *            上下文
	 */
	public static void setContext(WeBankContext weBankContext) {
		WeBankContextThreadLocal.set(weBankContext);

	}

	/**
	 * 获取当前线程的上下文
	 * 
	 * @return WeBankContext 系统上下文
	 */
	public static WeBankContext getContext() {
		return WeBankContextThreadLocal.get();
	}

	/**
	 * 删除当前线程对应的上下文
	 */
	public static void unsetContext() {
		WeBankContextThreadLocal.unset();
	}

	public static <E> void setBizValue(String key, E value) {
		WeBankContext weBankContext = WeBankContextThreadLocal.get();
		if (weBankContext == null) {
			weBankContext = new WeBankContext();
			setContext(weBankContext);
		}

		weBankContext.setBizValue(key, value);
	}

	@SuppressWarnings("unchecked")
	public static <E> E getBizValue(String key) {
		WeBankContext weBankContext = WeBankContextThreadLocal.get();
		E obj = null;
		if (weBankContext != null) {
			obj = (E) weBankContext.getBizValue(key);
		}

		return obj;
	}

	public static String getAppId() {
		WeBankContext weBankContext = WeBankContextThreadLocal.get();
		String appId = null;
		if (weBankContext != null) {
			appId = weBankContext.getAppId();
		}

		return appId;
	}

	public static void setAppId(String appId) {
		WeBankContext weBankContext = WeBankContextThreadLocal.get();

		if (weBankContext == null) {
			weBankContext = new WeBankContext();
			setContext(weBankContext);
		}
		weBankContext.setAppId(appId);

	}

}
