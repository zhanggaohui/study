package com.redis;

import java.util.HashSet;
import java.util.Set;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisSentinelPool;

public class MyJedisSentinelTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Set sentinels = new HashSet();
        sentinels.add(new HostAndPort("192.168.106.13", 26379).toString());
        sentinels.add(new HostAndPort("192.168.106.13", 26380).toString());
       // sentinels.add(new HostAndPort("localhost", 26381).toString());
        JedisSentinelPool sentinelPool = new JedisSentinelPool("mymaster", sentinels);
        System.out.println("Current master: " + sentinelPool.getCurrentHostMaster().toString());
        //sJedis master = sentinelPool.getResource();
       // master.set("username","liangzhichao");
       // sentinelPool.returnResource(master);
        Jedis master2 = sentinelPool.getResource();
        String value = master2.get("pp");
        System.out.println("username: " + value);
        master2.close();
        sentinelPool.destroy();
	}

}
