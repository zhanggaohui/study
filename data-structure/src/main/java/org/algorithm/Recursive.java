package org.algorithm;

/**
 * 递归算法
 * @author Administrator
 *
 */
public class Recursive {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int total = getTotal(12);
		System.out.println(total);
		
		double money = getMoney(47);
		System.out.println(money);
	}
	
	/**
	 * 
		裴波那契（fibonacci leonardo，约1170-1250）是意大利著名数学家．在他的著作《算盘书》中许多有趣的问题，最富成功的问题是著名的“兔子繁殖问题”： 如果每对兔子每月繁殖一对子兔，而子兔在出生后第二个月就有生殖能力，试问第一月有一对小兔子第十二月时有多少对兔子？
		1、1、2、3、5、8、13、21……
		假设第n个月的兔子数目为f(n)，那么
		   f(n)=f(n-1)+f(n-2)    当n≥3，
		   f(1)=f(2)=1  
		讨论得出算法描述
	 * @param n
	 * @return
	 */
	public static int getTotal(int n){
		if(n==0){
			return 1;
		}
		if(n ==1){
			return 1;
		}
		return getTotal(n-1)+getTotal(n-2);
	}
	
	/**
	 * 逆推法
	 * @param n
	 * @return
	 */
	public static double getMoney(int n){
		if(n==48) return 1000;
		
		return (getMoney(n+1)+1000)/(1+0.0171/12);
		
		
	}

}
